use crate::{daemon::Handle, views::CommandConfig};
use gtk::prelude::*;
use marble::widgets::Dialog;
use std::rc::Rc;

pub(crate) fn build(serial_number: String) -> Dialog {
    let disconnected_page = crate::widgets::disconnected_page();

    let press_label = gtk::Label::new(Some(
        "Please press the button you wish to program on your streamdeck",
    ));
    press_label
        .style_context()
        .add_class(marble::STYLE_CLASS_H2_LABEL);
    press_label.set_hexpand(true);
    press_label.set_max_width_chars(30);
    press_label.set_wrap(true);
    press_label.set_wrap_mode(pango::WrapMode::WordChar);
    press_label.set_xalign(0f32);

    let press_page = gtk::Grid::new();
    press_page.set_column_spacing(12);
    press_page.set_row_spacing(12);
    press_page.set_halign(gtk::Align::Center);
    press_page.set_margin(24);

    press_page.attach(&press_label, 0, 0, 1, 1);

    let existing_key_label = gtk::Label::new(Some("Key already mapped, try pressing a new key"));
    existing_key_label
        .style_context()
        .add_class(marble::STYLE_CLASS_H2_LABEL);
    existing_key_label.set_hexpand(true);
    existing_key_label.set_max_width_chars(3);
    existing_key_label.set_wrap(true);
    existing_key_label.set_wrap_mode(pango::WrapMode::WordChar);
    existing_key_label.set_xalign(0f32);

    let existing_key_page = gtk::Grid::new();
    existing_key_page.set_column_spacing(12);
    existing_key_page.set_row_spacing(12);
    existing_key_page.set_halign(gtk::Align::Center);
    existing_key_page.set_margin(24);

    existing_key_page.attach(&existing_key_label, 0, 0, 1, 1);

    let command_page = CommandConfig::new(&serial_number);

    let stack = gtk::Stack::new();
    stack.add_named(&disconnected_page, "disconnected");
    stack.add_named(&press_page, "keypress");
    stack.add_named(&existing_key_page, "existing");
    stack.add_named(&command_page, "command");

    let dialog = Dialog::new();
    dialog.content_area().add(&stack);

    dialog.show_all();

    read_input(&stack, &dialog, command_page, Rc::new(serial_number));

    dialog
}

fn read_input(
    stack: &gtk::Stack,
    dialog: &Dialog,
    command_page: CommandConfig,
    serial_number: Rc<String>,
) {
    glib::MainContext::default().spawn_local(
        glib::clone!(@weak stack, @weak dialog, @weak command_page => async move {
            let state = Handle::current().state().obs_state();
            if state.is_connected() {
                dialog.add_button("Close", gtk::ResponseType::Close);
                stack.set_visible_child_name("keypress");
            } else {
                dialog.add_button("Close", gtk::ResponseType::Reject);
                return;
            }

            let weak_stack = stack.downgrade();
            let weak_command_page = command_page.downgrade();
            drop(stack);
            drop(command_page);

            while let Some(stack) = weak_stack.upgrade() {
                let command_page = if let Some(command_page) = weak_command_page.upgrade() {
                    command_page
                } else {
                    break;
                };

                if let Ok(keypresses) = Handle::current().read_input().await {
                    let mut should_break = false;

                    for keypress in keypresses {
                        if keypress.serial_number == *serial_number {
                            should_break = true;
                            command_page.set_input_keys(keypress.keys);
                            stack.set_visible_child_name("command");
                        }
                    }

                    if should_break {
                        break;
                    }
                }
            }
        }),
    );
}
