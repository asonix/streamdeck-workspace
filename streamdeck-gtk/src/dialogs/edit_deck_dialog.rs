use crate::{daemon::Handle, widgets::DeckItem};
use gtk::prelude::*;
use marble::widgets::Dialog;
use std::{cell::RefCell, rc::Rc};

pub(crate) fn build(deck_item: &DeckItem) -> Option<Dialog> {
    let device_name = deck_item.device_name()?;
    let serial_number = deck_item.serial_number()?;

    let dialog = Dialog::new();

    let label = gtk::Label::new(Some("Edit deck name"));
    let entry = gtk::Entry::new();
    entry.set_valign(gtk::Align::Center);
    entry
        .style_context()
        .add_class(marble::STYLE_CLASS_H3_LABEL);
    entry.set_text(&device_name);

    let grid = gtk::Grid::new();
    grid.set_column_spacing(12);
    grid.set_row_spacing(12);
    grid.set_halign(gtk::Align::Center);
    grid.set_margin(24);

    grid.attach(&label, 0, 0, 1, 1);
    grid.attach(&entry, 1, 0, 3, 1);

    dialog.content_area().add(&grid);

    entry
        .bind_property("text", deck_item, "device-name")
        .build();

    let serial_number = Rc::new(serial_number);
    let previous_name = Rc::new(RefCell::new(device_name));
    entry.connect_changed(move |entry| {
        let serial_number = Rc::clone(&serial_number);
        let previous_name = Rc::clone(&previous_name);

        if previous_name.borrow().as_str() == entry.text() {
            return;
        }

        *previous_name.borrow_mut() = entry.text().into();

        glib::MainContext::default().spawn_local(async move {
            let _ = Handle::current()
                .set_deck_name(
                    String::clone(&serial_number),
                    String::clone(&previous_name.borrow()),
                )
                .await;
        });
    });

    dialog.add_button("Close", gtk::ResponseType::Close);

    dialog.show_all();
    Some(dialog)
}
