use crate::{
    daemon::{CommandState, Handle},
    views::CommandConfig,
};
use gtk::prelude::*;
use marble::widgets::Dialog;

pub(crate) fn build(serial_number: &str, command_state: CommandState) -> Dialog {
    let disconnected_page = crate::widgets::disconnected_page();

    let command_page = CommandConfig::new(serial_number);

    let stack = gtk::Stack::new();
    stack.add_named(&disconnected_page, "disconnected");
    stack.add_named(&command_page, "command");

    let dialog = Dialog::new();
    dialog.content_area().add(&stack);

    dialog.show_all();

    command_page.register_command(command_state);

    if Handle::current().state().obs_state().is_connected() {
        dialog.add_button("Close", gtk::ResponseType::Close);
        stack.set_visible_child_name("command");
    } else {
        dialog.add_button("Close", gtk::ResponseType::Reject);
    }

    dialog
}
