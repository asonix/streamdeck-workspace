use crate::{daemon::DeckState, widgets::CommandList};
use gtk::prelude::*;

glib::wrapper! {
    pub struct DeckStack(ObjectSubclass<imp::DeckStack>)
        @extends gtk::Stack, gtk::Container, gtk::Widget;
}

impl DeckStack {
    pub(crate) fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create DeckStack")
    }

    pub(crate) fn select_deck(&self, serial_number: &str) {
        if self.child_by_name(serial_number).is_some() {
            self.set_visible_child_name(serial_number);
        }
        self.show_all();
    }

    pub(crate) fn add_deck(&self, deck_state: DeckState) {
        if self.child_by_name(&deck_state.serial_number()).is_none() {
            let command_list = CommandList::new(deck_state.clone());
            self.add_named(&command_list, &deck_state.serial_number());
        }
    }

    pub(crate) fn remove_deck(&self, serial_number: &str) {
        if let Some(child) = self.child_by_name(serial_number) {
            self.remove(&child);
        }
    }
}

mod imp {
    use gtk::{prelude::*, subclass::prelude::*};

    #[derive(Debug, Default)]
    pub struct DeckStack;

    #[glib::object_subclass]
    impl ObjectSubclass for DeckStack {
        const NAME: &'static str = "DeckStack";
        type Type = super::DeckStack;
        type ParentType = gtk::Stack;
    }

    impl ObjectImpl for DeckStack {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.add_named(&gtk::Label::new(Some("Empty State")), "empty-state");

            obj.show_all();
        }
    }

    impl WidgetImpl for DeckStack {}
    impl ContainerImpl for DeckStack {}
    impl StackImpl for DeckStack {}
}
