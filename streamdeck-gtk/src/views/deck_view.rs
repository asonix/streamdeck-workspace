glib::wrapper! {
    pub struct DeckView(ObjectSubclass<imp::DeckView>)
        @extends gtk::Box, gtk::Container, gtk::Widget;
}

impl DeckView {
    pub(crate) fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create deckview")
    }
}

mod imp {
    use crate::daemon::Handle;
    use futures_util::stream::StreamExt;
    use gtk::{prelude::*, subclass::prelude::*};

    #[derive(Debug, Default)]
    pub struct DeckView;

    #[glib::object_subclass]
    impl ObjectSubclass for DeckView {
        const NAME: &'static str = "DeckView";
        type Type = super::DeckView;
        type ParentType = gtk::Box;
    }

    impl ObjectImpl for DeckView {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.set_expand(true);

            let deck_stack = crate::views::DeckStack::new();
            deck_stack.set_margin(12);
            deck_stack.set_margin_start(0);

            let deck_list = crate::widgets::DeckList::new(&deck_stack);

            for deck_state in Handle::current().state().decks() {
                deck_list.add_deck(deck_state);
            }
            if !deck_list.any_selected() {
                deck_list.select_first_item();
            }

            let added = Handle::current().state().added_deck_stream();
            glib::MainContext::default().spawn_local(glib::clone!(@weak deck_list => async move {
                futures_util::pin_mut!(added);
                while let Some(deck_state) = added.next().await {
                    deck_list.add_deck(deck_state);

                    if !deck_list.any_selected() {
                        deck_list.select_first_item();
                    }

                    deck_list.show_all();
                }
            }));

            let removed = Handle::current().state().removed_deck_stream();
            glib::MainContext::default().spawn_local(glib::clone!(@weak deck_list => async move {
                futures_util::pin_mut!(removed);
                while let Some(key) = removed.next().await {
                    deck_list.remove_deck(key);

                    if !deck_list.any_selected() {
                        deck_list.select_first_item();
                    }

                    deck_list.show_all();
                }
            }));

            let scroll = gtk::ScrolledWindow::builder().expand(true).build();
            scroll.add(&deck_list);

            let sidebar = gtk::Grid::new();
            sidebar.set_orientation(gtk::Orientation::Vertical);
            sidebar.set_margin(12);
            sidebar.set_margin_end(0);
            sidebar.add(&scroll);

            let grid = gtk::Grid::new();
            grid.set_column_homogeneous(true);
            grid.set_column_spacing(12);
            grid.set_row_spacing(12);

            grid.attach(&sidebar, 0, 0, 1, 1);
            grid.attach(&deck_stack, 1, 0, 2, 1);

            obj.add(&grid);

            obj.show_all();
        }
    }

    impl WidgetImpl for DeckView {}
    impl ContainerImpl for DeckView {}
    impl BoxImpl for DeckView {}
}
