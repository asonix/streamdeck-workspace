use gtk::subclass::prelude::*;
use streamdeck_common::ObsState;

glib::wrapper! {
    pub struct Obs(ObjectSubclass<imp::Obs>)
        @extends gtk::Box, gtk::Bin, gtk::Container, gtk::Widget;
}

impl Obs {
    pub(crate) fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create obs")
    }

    async fn on_click(&self) {
        let this = imp::Obs::from_instance(self);
        this.on_click().await;
    }

    async fn on_state(&self, state: ObsState) {
        let this = imp::Obs::from_instance(self);
        this.on_state(state).await;
    }
}

mod imp {
    use crate::daemon::Handle;
    use futures_util::stream::StreamExt;
    use gio::prelude::SettingsExt;
    use gtk::{prelude::*, subclass::prelude::*};
    use once_cell::unsync::OnceCell;
    use streamdeck_common::ObsState;

    #[derive(Default)]
    pub struct Obs {
        connect_button: OnceCell<gtk::Button>,
        auth_label_revealer: OnceCell<gtk::Revealer>,
        auth_entry_revealer: OnceCell<gtk::Revealer>,
        auth_entry: OnceCell<gtk::Entry>,
        err_stack: OnceCell<gtk::Stack>,
        err_revealer: OnceCell<gtk::Revealer>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Obs {
        const NAME: &'static str = "Obs";
        type Type = super::Obs;
        type ParentType = gtk::Box;
    }

    impl ObjectImpl for Obs {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            let host_label = gtk::Label::new(Some("Hostname"));
            let host_entry = gtk::Entry::new();
            host_entry.set_valign(gtk::Align::Center);
            host_entry
                .style_context()
                .add_class(marble::STYLE_CLASS_H3_LABEL);

            let port_label = gtk::Label::new(Some("Port"));
            let port_entry = gtk::SpinButton::with_range(1f64, 65535f64, 1f64);
            port_entry.set_valign(gtk::Align::Center);
            port_entry.set_snap_to_ticks(true);

            let obs_settings = crate::obs_settings();

            obs_settings
                .bind("host", &host_entry, "text")
                .flags(gio::SettingsBindFlags::DEFAULT)
                .build();
            obs_settings
                .bind("port", &port_entry, "value")
                .flags(gio::SettingsBindFlags::DEFAULT)
                .build();

            let connect_button = gtk::Button::new();

            let auth_label = gtk::Label::new(Some("Password"));
            let auth_entry = gtk::Entry::new();
            auth_entry.set_valign(gtk::Align::Center);
            auth_entry.set_visibility(false);
            auth_entry.set_input_purpose(gtk::InputPurpose::Password);
            auth_entry
                .style_context()
                .add_class(marble::STYLE_CLASS_H3_LABEL);

            let auth_label_revealer = gtk::Revealer::new();
            auth_label_revealer.set_child(Some(&auth_label));
            auth_label_revealer.set_reveal_child(false);

            let auth_entry_revealer = gtk::Revealer::new();
            auth_entry_revealer.set_child(Some(&auth_entry));
            auth_entry_revealer.set_reveal_child(false);

            let connect_err =
                gtk::Label::new(Some("Failed to connect to OBS with provided information"));

            let auth_err =
                gtk::Label::new(Some("Failed to authenticate to OBS with provided password"));

            let err_stack = gtk::Stack::new();
            err_stack.add_named(&connect_err, "connect");
            err_stack.add_named(&auth_err, "auth");

            let err_revealer = gtk::Revealer::new();
            err_revealer.set_child(Some(&err_stack));
            err_revealer.set_reveal_child(false);

            let grid = gtk::Grid::new();
            grid.set_column_spacing(12);
            grid.set_row_spacing(12);
            grid.set_halign(gtk::Align::Center);
            grid.set_margin_top(64);

            grid.attach(&host_label, 0, 0, 1, 1);
            grid.attach(&host_entry, 1, 0, 1, 1);
            grid.attach(&port_label, 0, 1, 1, 1);
            grid.attach(&port_entry, 1, 1, 1, 1);
            grid.attach(&auth_label_revealer, 0, 2, 1, 1);
            grid.attach(&auth_entry_revealer, 1, 2, 1, 1);
            grid.attach(&connect_button, 1, 3, 1, 1);
            grid.attach(&err_revealer, 0, 4, 2, 1);

            obj.set_halign(gtk::Align::Center);
            obj.add(&grid);

            obj.show_all();

            let weak = obj.downgrade();
            glib::MainContext::default().spawn_local(async move {
                let stream = Handle::current().state().obs_state_stream();
                futures_util::pin_mut!(stream);

                while let Some(state) = stream.next().await {
                    if let Some(obj) = weak.upgrade() {
                        obj.on_state(state).await;
                    } else {
                        break;
                    }
                }
            });

            connect_button.connect_clicked(glib::clone!(@weak obj => move |_| {
                glib::MainContext::default()
                    .spawn_local(glib::clone!(@weak obj => async move {
                        obj.on_click().await;
                    }));
            }));

            match Handle::current().state().obs_state() {
                ObsState::Disconnected => {
                    connect_button.set_label("Connect");
                }
                ObsState::Unauthenticated => {
                    connect_button.set_label("Login");
                }
                ObsState::Connected => {
                    connect_button.set_label("Disconnect");
                }
            }

            self.connect_button.set(connect_button).unwrap();
            self.auth_label_revealer.set(auth_label_revealer).unwrap();
            self.auth_entry_revealer.set(auth_entry_revealer).unwrap();
            self.auth_entry.set(auth_entry).unwrap();
            self.err_stack.set(err_stack).unwrap();
            self.err_revealer.set(err_revealer).unwrap();
        }
    }

    impl WidgetImpl for Obs {}
    impl ContainerImpl for Obs {}
    impl BinImpl for Obs {}
    impl BoxImpl for Obs {}

    impl std::fmt::Debug for Obs {
        fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
            write!(f, "Obs")
        }
    }

    impl Obs {
        pub(super) async fn on_state(&self, state: ObsState) {
            let connect_button = self.connect_button.get().unwrap();
            let auth_label_revealer = self.auth_label_revealer.get().unwrap();
            let auth_entry_revealer = self.auth_entry_revealer.get().unwrap();

            match state {
                ObsState::Disconnected => {
                    connect_button.set_label("Connect");
                }
                ObsState::Unauthenticated => {
                    auth_label_revealer.set_reveal_child(true);
                    auth_entry_revealer.set_reveal_child(true);
                    connect_button.set_label("Login");
                }
                ObsState::Connected => {
                    connect_button.set_label("Disconnect");
                }
            }
        }

        pub(super) async fn on_click(&self) {
            let connect_button = self.connect_button.get().unwrap();
            let auth_entry = self.auth_entry.get().unwrap();
            let err_stack = self.err_stack.get().unwrap();
            let err_revealer = self.err_revealer.get().unwrap();

            connect_button.set_sensitive(false);
            let before_state = Handle::current().state().obs_state();

            match before_state {
                ObsState::Disconnected => {
                    let host = crate::obs_settings().string("host").to_string();
                    if let Some(port) = crate::obs_settings().value("port").get() {
                        let _ = Handle::current().connect(host, port).await;
                    }
                }
                ObsState::Unauthenticated => {
                    let _ = Handle::current().login(auth_entry.text().into()).await;
                }
                ObsState::Connected => {
                    let _ = Handle::current().disconnect().await;
                }
            }

            let after_state = Handle::current().state().obs_state();

            if before_state == after_state {
                match before_state {
                    ObsState::Disconnected => {
                        err_stack.set_visible_child_name("connect");
                    }
                    ObsState::Unauthenticated => {
                        err_stack.set_visible_child_name("auth");
                    }
                    ObsState::Connected => {}
                }
                err_revealer.set_reveal_child(true);
                err_revealer.show_all();
            } else {
                err_revealer.set_reveal_child(false);
                err_revealer.show_all();
            }
            connect_button.set_sensitive(true);
        }
    }
}
