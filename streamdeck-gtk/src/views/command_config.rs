use crate::daemon::{CommandState, Handle};
use futures_util::stream::StreamExt;
use gtk::{prelude::*, subclass::prelude::*};
use streamdeck_common::{Command, CommandVariant, Input, StateOperation, VisibilityOperation};

glib::wrapper! {
    pub struct CommandConfig(ObjectSubclass<imp::CommandConfig>)
        @extends gtk::Box, gtk::Bin, gtk::Container, gtk::Widget;
}

impl CommandConfig {
    pub(crate) fn new(serial_number: &str) -> Self {
        let this: Self = glib::Object::new(&[]).expect("Failed to create command config");
        let inner = imp::CommandConfig::from_instance(&this);
        inner.serial_number.set(serial_number.to_owned()).unwrap();

        this
    }

    pub(crate) fn set_input_keys(&self, key: Input) {
        {
            let this = imp::CommandConfig::from_instance(self);
            this.input_key.set(key).unwrap();
        }

        let _ = self.persist_command();
    }

    pub(crate) fn register_command(&self, command_state: CommandState) {
        self.set_command(command_state.command());
        self.set_name(command_state.name());
        self.set_input_keys(command_state.keys());

        let stream = command_state.command_stream();
        let weak = self.downgrade();
        glib::MainContext::default().spawn_local(async move {
            futures_util::pin_mut!(stream);

            while let Some(command) = stream.next().await {
                if let Some(obj) = weak.upgrade() {
                    obj.set_command(command);
                } else {
                    break;
                }
            }
        });
    }

    fn set_name(&self, name: String) {
        let this = imp::CommandConfig::from_instance(self);
        this.command_name.get().unwrap().set_text(&name);
        *this.previous_name.borrow_mut() = name;
    }

    fn set_command(&self, command: Command) {
        {
            let this = imp::CommandConfig::from_instance(self);
            *this.command.borrow_mut() = Some(command.clone());
            this.command_combobox
                .get()
                .unwrap()
                .select(command.to_variant());
        }

        match &command {
            Command::SwitchScene { ref to } => {
                self.scene_selected(to);
            }
            Command::SetSceneItemVisibility {
                ref scene_name,
                item_id,
                operation,
            } => {
                let scene_name = scene_name.clone();
                let item_id = *item_id;
                let operation = *operation;

                let obj = self.clone();
                glib::MainContext::default().spawn_local(async move {
                    obj.scene_item_visibility_selected(scene_name, item_id, operation)
                        .await;
                });
            }
            Command::SetRecording { operation } => {
                self.recording_selected(*operation);
            }
            Command::SetStreaming { operation } => {
                self.streaming_selected(*operation);
            }
        }

        {
            let this = imp::CommandConfig::from_instance(self);
            this.command_stack
                .get()
                .unwrap()
                .set_visible_child_name(command.to_variant().as_id());
        }
    }

    async fn selected(&self, cmd_type: CommandVariant) {
        {
            let this = imp::CommandConfig::from_instance(self);
            let is_current = this
                .command
                .borrow()
                .as_ref()
                .map(|c| c.to_variant() == cmd_type)
                .unwrap_or(false);

            if is_current {
                return;
            }

            if let Some(cmd) = crate::command::to_default_command(cmd_type).await {
                *this.command.borrow_mut() = Some(cmd);

                this.command_stack
                    .get()
                    .unwrap()
                    .set_visible_child_name(cmd_type.as_id());
            }
        }

        let _ = self.persist_command();
    }

    fn scene_selected(&self, scene_name: &str) {
        let changed = {
            let this = imp::CommandConfig::from_instance(self);

            let changed = {
                let mut cmd = this.command.borrow_mut();
                let changed_opt = cmd.as_mut().map(|cmd| match cmd {
                    Command::SwitchScene { ref mut to } => {
                        let changed = to != scene_name;
                        *to = scene_name.to_owned();
                        changed
                    }
                    _ => false,
                });

                changed_opt.unwrap_or(false)
            };

            let scene_combobox = &this.switch_scene.get().unwrap().combobox;
            if scene_combobox
                .active_id()
                .map(|id| id != scene_name)
                .unwrap_or(true)
            {
                scene_combobox.set_active_id(Some(scene_name));
            }

            changed
        };

        if changed {
            let _ = self.persist_command();
        }
    }

    async fn scene_item_visibility_selected(
        &self,
        new_scene_name: String,
        new_item_id: i64,
        new_operation: VisibilityOperation,
    ) {
        let changed = {
            let this = imp::CommandConfig::from_instance(self);

            let changed = {
                let mut cmd = this.command.borrow_mut();

                let changed_opt = cmd.as_mut().map(|cmd| match cmd {
                    Command::SetSceneItemVisibility {
                        ref mut scene_name,
                        ref mut item_id,
                        ref mut operation,
                    } => {
                        let changed = *scene_name != new_scene_name
                            || new_item_id != *item_id
                            || new_operation != *operation;

                        *scene_name = new_scene_name.clone();
                        *item_id = new_item_id;
                        *operation = new_operation;

                        changed
                    }
                    _ => false,
                });

                changed_opt.unwrap_or(false)
            };

            let state = this.set_scene_item_visibility.get().unwrap();
            if state
                .scene_combobox
                .active_id()
                .map(|ai| ai != new_scene_name)
                .unwrap_or(true)
            {
                state.scene_combobox.set_active_id(Some(&new_scene_name));
            }

            if state
                .item_combobox
                .active_id()
                .map(|ai| ai != new_item_id.to_string())
                .unwrap_or(true)
            {
                state
                    .item_combobox
                    .set_active_id(Some(&new_item_id.to_string()));
            }

            if state
                .op_combobox
                .active_id()
                .map(|ai| ai != new_operation.as_id())
                .unwrap_or(true)
            {
                state.op_combobox.set_active_id(Some(new_operation.as_id()));
            }

            changed
        };

        if changed {
            let _ = self.persist_command();
        }
    }

    fn recording_selected(&self, new_operation: StateOperation) {
        let changed = {
            let this = imp::CommandConfig::from_instance(self);

            let changed = {
                let mut cmd = this.command.borrow_mut();

                let changed_opt = cmd.as_mut().map(|cmd| match cmd {
                    Command::SetRecording { ref mut operation } => {
                        let changed = *operation != new_operation;
                        *operation = new_operation;
                        changed
                    }
                    _ => false,
                });

                changed_opt.unwrap_or(false)
            };

            let combobox = &this.set_recording.get().unwrap().combobox;
            if combobox
                .active_id()
                .map(|ai| ai != new_operation.as_id())
                .unwrap_or(true)
            {
                combobox.set_active_id(Some(new_operation.as_id()));
            }

            changed
        };

        if changed {
            let _ = self.persist_command();
        }
    }

    fn streaming_selected(&self, new_operation: StateOperation) {
        let changed = {
            let this = imp::CommandConfig::from_instance(self);

            let changed = {
                let mut cmd = this.command.borrow_mut();

                let changed_opt = cmd.as_mut().map(|cmd| match cmd {
                    Command::SetStreaming { ref mut operation } => {
                        let changed = *operation != new_operation;
                        *operation = new_operation;
                        changed
                    }
                    _ => false,
                });

                changed_opt.unwrap_or(false)
            };

            let combobox = &this.set_streaming.get().unwrap().combobox;
            if combobox
                .active_id()
                .map(|ai| ai != new_operation.as_id())
                .unwrap_or(true)
            {
                combobox.set_active_id(Some(new_operation.as_id()));
            }

            changed
        };

        if changed {
            let _ = self.persist_command();
        }
    }

    fn scene_item_visibility_changed(&self) -> Option<()> {
        let (scene_name, item_id, operation) = {
            let this = imp::CommandConfig::from_instance(self);
            let state = this.set_scene_item_visibility.get()?;
            let scene_name = state.scene_combobox.active_id()?;
            let item_id = state.item_combobox.active_id()?.parse().ok()?;
            let operation = state.op_combobox.active_id()?.parse().ok()?;

            (scene_name, item_id, operation)
        };

        let obj = self.clone();
        glib::MainContext::default().spawn_local(async move {
            obj.scene_item_visibility_selected(scene_name.to_string(), item_id, operation)
                .await;
        });
        Some(())
    }

    fn persist_command(&self) -> Option<()> {
        let this = imp::CommandConfig::from_instance(self);
        let serial_number = this.serial_number.get()?.to_owned();
        let key = this.input_key.get()?.to_owned();

        let cmd = this.command.borrow().as_ref()?.clone();

        glib::MainContext::default().spawn_local(async move {
            let _ = Handle::current().set_input(serial_number, key, &cmd).await;
        });

        Some(())
    }

    fn persist_name(&self) {
        let this = imp::CommandConfig::from_instance(self);
        if this.configure_id.borrow().is_some() {
            return;
        }

        let new_configure_id = glib::source::timeout_add_local(
            std::time::Duration::from_millis(200),
            glib::clone!(@weak-allow-none self as obj => move || {
                if let Some(obj) = obj {
                    let _ = obj.do_persist_name();
                }

                glib::source::Continue(false)
            }),
        );

        *this.configure_id.borrow_mut() = Some(new_configure_id);
    }

    fn do_persist_name(&self) -> Option<()> {
        let this = imp::CommandConfig::from_instance(self);
        *this.configure_id.borrow_mut() = None;
        let mut previous_name = this.previous_name.borrow_mut();
        let serial_number = this.serial_number.get()?.to_owned();
        let key = this.input_key.get()?.to_owned();
        let name = this.command_name.get()?.text().to_string();

        if *previous_name != name {
            *previous_name = name.clone();
            glib::MainContext::default().spawn_local(async move {
                let _ = Handle::current()
                    .set_input_name(serial_number, key, name)
                    .await;
            });
        }

        None
    }
}

mod imp {
    use crate::{command::ComboBoxHandle, daemon::Handle};
    use futures_util::stream::StreamExt;
    use gtk::{prelude::*, subclass::prelude::*};
    use once_cell::unsync::OnceCell;
    use std::cell::RefCell;
    use streamdeck_common::{Command, CommandVariant, Input, StateOperation, VisibilityOperation};

    #[derive(Debug)]
    pub(super) struct SwitchSceneState {
        pub(super) combobox: gtk::ComboBoxText,
    }

    #[derive(Debug)]
    pub(super) struct SetSceneItemVisibilityState {
        pub(super) scene_combobox: gtk::ComboBoxText,
        pub(super) item_combobox: gtk::ComboBoxText,
        pub(super) op_combobox: gtk::ComboBoxText,
    }

    #[derive(Debug)]
    pub(super) struct SetRecordingState {
        pub(super) combobox: gtk::ComboBoxText,
    }

    #[derive(Debug)]
    pub(super) struct SetStreamingState {
        pub(super) combobox: gtk::ComboBoxText,
    }

    #[derive(Debug, Default)]
    pub struct CommandConfig {
        pub(super) serial_number: OnceCell<String>,
        pub(super) input_key: OnceCell<Input>,
        pub(super) command: RefCell<Option<Command>>,
        pub(super) command_stack: OnceCell<gtk::Stack>,
        pub(super) command_name: OnceCell<gtk::Entry>,
        pub(super) previous_name: RefCell<String>,
        pub(super) configure_id: RefCell<Option<glib::source::SourceId>>,
        pub(super) command_combobox: OnceCell<ComboBoxHandle>,
        pub(super) switch_scene: OnceCell<SwitchSceneState>,
        pub(super) set_scene_item_visibility: OnceCell<SetSceneItemVisibilityState>,
        pub(super) set_recording: OnceCell<SetRecordingState>,
        pub(super) set_streaming: OnceCell<SetStreamingState>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CommandConfig {
        const NAME: &'static str = "CommandConfig";
        type Type = super::CommandConfig;
        type ParentType = gtk::Box;
    }

    impl ObjectImpl for CommandConfig {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            let command_title = gtk::Label::new(Some("Configure the command you wish send"));
            command_title.set_hexpand(true);
            command_title
                .style_context()
                .add_class(marble::STYLE_CLASS_H2_LABEL);
            command_title.set_max_width_chars(75);
            command_title.set_wrap(true);
            command_title.set_wrap_mode(pango::WrapMode::WordChar);
            command_title.set_xalign(0.0);
            command_title.set_margin_bottom(24);

            let command_label = styled_label(Some("Command:"));

            let (command_combobox, handle) = crate::command::render_config(None);

            if let Some(cmd_type) = handle.selected() {
                glib::MainContext::default().spawn_local(glib::clone!(@weak obj => async move {
                    obj.selected(cmd_type).await;
                }));
            }

            if let Some(mut stream) = handle.selected_stream() {
                let obj = obj.downgrade();
                glib::MainContext::default().spawn_local(async move {
                    while let Some(cmd_type) = stream.next().await {
                        if let Some(obj) = obj.upgrade() {
                            obj.selected(cmd_type).await;
                        } else {
                            break;
                        }
                    }
                });
            }

            self.command_combobox.set(handle).unwrap();

            let command_stack = gtk::Stack::new();
            command_stack.add_named(&gtk::Label::new(Some("...")), "empty");
            self.configure_scene_switcher(&command_stack, obj);
            self.configure_scene_item_visibility(&command_stack, obj);
            self.configure_recording(&command_stack, obj);
            self.configure_streaming(&command_stack, obj);

            let name_label = styled_label(Some("Name:"));
            let name_entry = gtk::Entry::new();
            name_entry.set_valign(gtk::Align::Center);
            name_entry.set_halign(gtk::Align::Start);
            name_entry.set_hexpand(true);
            name_entry
                .style_context()
                .add_class(marble::STYLE_CLASS_H3_LABEL);

            name_entry.connect_changed(glib::clone!(@weak obj => move |_| {
                obj.persist_name();
            }));

            let grid = gtk::Grid::new();
            grid.set_column_spacing(12);
            grid.set_row_spacing(6);
            grid.set_halign(gtk::Align::Center);
            grid.set_valign(gtk::Align::Center);
            grid.set_vexpand(true);
            grid.set_margin(24);

            grid.attach(&command_title, 0, 0, 3, 1);
            grid.attach(&name_label, 0, 1, 1, 1);
            grid.attach(&name_entry, 1, 1, 2, 1);
            grid.attach(&command_label, 0, 2, 1, 1);
            grid.attach(&command_combobox, 1, 2, 2, 1);
            grid.attach(&command_stack, 0, 3, 3, 1);

            obj.set_halign(gtk::Align::Center);
            obj.add(&grid);

            obj.show_all();

            *self.previous_name.borrow_mut() = name_entry.text().to_string();
            self.command_stack.set(command_stack).unwrap();
            self.command_name.set(name_entry).unwrap();
        }
    }

    impl WidgetImpl for CommandConfig {}
    impl ContainerImpl for CommandConfig {}
    impl BinImpl for CommandConfig {}
    impl BoxImpl for CommandConfig {}

    impl CommandConfig {
        fn populate_streaming(&self) -> Option<()> {
            let state = self.set_streaming.get()?;

            for operation in StateOperation::all() {
                state
                    .combobox
                    .append(Some(operation.as_id()), operation.as_id());
            }

            let operation = self
                .command
                .borrow()
                .as_ref()
                .and_then(|cmd| match cmd {
                    Command::SetStreaming { operation } => Some(*operation),
                    _ => None,
                })
                .or_else(|| StateOperation::all().get(0).copied())?;

            state.combobox.set_active_id(Some(operation.as_id()));

            Some(())
        }

        fn configure_streaming(
            &self,
            command_stack: &gtk::Stack,
            obj: &<Self as ObjectSubclass>::Type,
        ) {
            let streaming_label = styled_label(Some("State:"));

            let streaming_combobox = gtk::ComboBoxText::new();
            streaming_combobox.set_id_column(1);

            let grid = styled_grid();
            grid.attach(&streaming_label, 0, 0, 1, 1);
            grid.attach(&streaming_combobox, 1, 0, 1, 1);

            command_stack.add_named(&grid, CommandVariant::SetStreaming.as_id());

            streaming_combobox.connect_changed(glib::clone!(@weak obj => move |combobox| {
                if let Some(operation) = combobox.active_id() {
                    if let Ok(operation) = operation.parse() {
                        obj.streaming_selected(operation);
                    }
                }
            }));

            self.set_streaming
                .set(SetStreamingState {
                    combobox: streaming_combobox,
                })
                .unwrap();

            self.populate_streaming();
        }

        fn populate_recording(&self) -> Option<()> {
            let state = self.set_recording.get()?;

            for operation in StateOperation::all() {
                state
                    .combobox
                    .append(Some(operation.as_id()), operation.as_id());
            }

            let operation = self
                .command
                .borrow()
                .as_ref()
                .and_then(|cmd| match cmd {
                    Command::SetRecording { operation } => Some(*operation),
                    _ => None,
                })
                .or_else(|| StateOperation::all().get(0).copied())?;

            state.combobox.set_active_id(Some(operation.as_id()));

            Some(())
        }

        fn configure_recording(
            &self,
            command_stack: &gtk::Stack,
            obj: &<Self as ObjectSubclass>::Type,
        ) {
            let recording_label = styled_label(Some("State:"));

            let recording_combobox = gtk::ComboBoxText::new();
            recording_combobox.set_id_column(1);

            let grid = styled_grid();
            grid.attach(&recording_label, 0, 0, 1, 1);
            grid.attach(&recording_combobox, 1, 0, 1, 1);

            command_stack.add_named(&grid, CommandVariant::SetRecording.as_id());

            recording_combobox.connect_changed(glib::clone!(@weak obj => move |combobox| {
                if let Some(operation) = combobox.active_id() {
                    if let Ok(operation) = operation.parse() {
                        obj.recording_selected(operation);
                    }
                }
            }));

            self.set_recording
                .set(SetRecordingState {
                    combobox: recording_combobox,
                })
                .unwrap();

            self.populate_recording();
        }

        async fn populate_scene_item_visibility(&self) -> Option<()> {
            let state = self.set_scene_item_visibility.get()?;

            let opt = self.command.borrow().as_ref().and_then(|cmd| match cmd {
                Command::SetSceneItemVisibility {
                    ref scene_name,
                    item_id,
                    operation,
                } => Some((scene_name.clone(), *item_id, *operation)),
                _ => None,
            });

            let scenes = Handle::current().get_scenes().await.ok()?;

            let scene = opt
                .as_ref()
                .map(|tup| &tup.0)
                .or_else(|| scenes.get(0).map(|scene| &scene.name))?;

            let items = Handle::current()
                .get_scene_items(scene.to_owned())
                .await
                .ok()?;

            let item_id = opt
                .as_ref()
                .map(|tup| tup.1)
                .or_else(|| items.get(0).map(|item| item.id))?;

            let operation = opt
                .as_ref()
                .map(|tup| tup.2)
                .unwrap_or(VisibilityOperation::Toggle);

            for scene in &scenes {
                state.scene_combobox.append(Some(&scene.name), &scene.name);
            }

            for item in &items {
                state
                    .item_combobox
                    .append(Some(&item.id.to_string()), &item.name);
            }

            for op in VisibilityOperation::all() {
                state.op_combobox.append(Some(op.as_id()), op.as_id());
            }

            state.scene_combobox.set_active_id(Some(scene));
            state
                .item_combobox
                .set_active_id(Some(&item_id.to_string()));
            state.op_combobox.set_active_id(Some(operation.as_id()));

            Some(())
        }

        fn configure_scene_item_visibility(
            &self,
            command_stack: &gtk::Stack,
            obj: &<Self as ObjectSubclass>::Type,
        ) {
            let scene_label = styled_label(Some("Scene:"));

            let scene_combobox = gtk::ComboBoxText::new();
            scene_combobox.set_id_column(1);

            let item_label = styled_label(Some("Item:"));

            let item_combobox = gtk::ComboBoxText::new();
            item_combobox.set_id_column(1);

            let op_label = styled_label(Some("Operation:"));

            let op_combobox = gtk::ComboBoxText::new();
            op_combobox.set_id_column(1);

            let grid = styled_grid();
            grid.attach(&scene_label, 0, 0, 1, 1);
            grid.attach(&scene_combobox, 1, 0, 1, 1);
            grid.attach(&item_label, 0, 1, 1, 1);
            grid.attach(&item_combobox, 1, 1, 1, 1);
            grid.attach(&op_label, 0, 2, 1, 1);
            grid.attach(&op_combobox, 1, 2, 1, 1);

            command_stack.add_named(&grid, CommandVariant::SetSceneItemVisibility.as_id());

            glib::MainContext::default().spawn_local(glib::clone!(@weak obj => async move {
                let this = CommandConfig::from_instance(&obj);
                this.populate_scene_item_visibility().await;
            }));

            scene_combobox.connect_changed(glib::clone!(@weak obj => move |_| {
                obj.scene_item_visibility_changed();
            }));

            item_combobox.connect_changed(glib::clone!(@weak obj => move |_| {
                obj.scene_item_visibility_changed();
            }));

            op_combobox.connect_changed(glib::clone!(@weak obj => move |_| {
                obj.scene_item_visibility_changed();
            }));

            self.set_scene_item_visibility
                .set(SetSceneItemVisibilityState {
                    scene_combobox,
                    item_combobox,
                    op_combobox,
                })
                .unwrap();
        }

        async fn populate_scene_switcher(&self) {
            let combobox = &self.switch_scene.get().unwrap().combobox;

            let scenes = if let Ok(scenes) = Handle::current().get_scenes().await {
                scenes
            } else {
                return;
            };

            for scene in &scenes {
                combobox.append(Some(&scene.name), &scene.name);
            }

            let scene_opt = self.command.borrow().as_ref().and_then(|cmd| match cmd {
                Command::SwitchScene { ref to } => Some(to.clone()),
                _ => None,
            });

            if let Some(scene) = scene_opt.as_ref() {
                combobox.set_active_id(Some(scene));
                return;
            }

            if let Some(scene) = scenes.get(0) {
                combobox.set_active_id(Some(&scene.name));
            }
        }

        fn configure_scene_switcher(
            &self,
            command_stack: &gtk::Stack,
            obj: &<Self as ObjectSubclass>::Type,
        ) {
            let scenes_label = styled_label(Some("Switch scene to"));

            let combobox = gtk::ComboBoxText::new();
            combobox.set_id_column(1);

            let scene_grid = styled_grid();

            scene_grid.attach(&scenes_label, 0, 0, 1, 1);
            scene_grid.attach(&combobox, 1, 0, 1, 1);

            command_stack.add_named(&scene_grid, CommandVariant::SwitchScene.as_id());

            glib::MainContext::default().spawn_local(glib::clone!(@weak obj => async move {
                let this = CommandConfig::from_instance(&obj);
                this.populate_scene_switcher().await;
            }));

            combobox.connect_changed(glib::clone!(@weak obj => move |combobox| {
                if let Some(scene_name) = combobox.active_id() {
                    obj.scene_selected(&scene_name);
                }
            }));

            self.switch_scene
                .set(SwitchSceneState { combobox })
                .unwrap();
        }
    }

    fn styled_label(text: Option<&str>) -> gtk::Label {
        let label = gtk::Label::new(text);
        label.set_halign(gtk::Align::End);
        label.set_valign(gtk::Align::Center);
        label
    }

    fn styled_grid() -> gtk::Grid {
        let grid = gtk::Grid::new();
        grid.set_margin_top(24);
        grid.set_column_spacing(12);
        grid.set_row_spacing(12);
        grid.set_halign(gtk::Align::Center);
        grid.set_valign(gtk::Align::Start);

        grid
    }
}
