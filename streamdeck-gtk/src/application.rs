use crate::main_window::MainWindow;
use gtk::prelude::*;
use once_cell::unsync::OnceCell;

pub(crate) fn main_window() -> Option<MainWindow> {
    thread_local! {
        static MAIN_WINDOW: OnceCell<MainWindow> = OnceCell::new();
    }

    MAIN_WINDOW.with(|window| window.get().map(Clone::clone))
}

glib::wrapper! {
    pub struct App(ObjectSubclass<imp::App>)
        @extends gtk::Application, gio::Application, gio::ActionMap;
}

impl App {
    pub(crate) fn new() -> Self {
        glib::Object::new(&[
            ("flags", &gio::ApplicationFlags::default()),
            ("application-id", &Some(crate::config::APP_ID)),
        ])
        .expect("Failed to create application")
    }

    fn setup_actions(&self, win: &MainWindow) {
        let action = gio::SimpleAction::new("quit", None);
        action.connect_activate(glib::clone!(@weak self as this, @weak win => move |_, _| {
            win.close();
            this.quit();
        }));
        self.add_action(&action);
        self.set_accels_for_action("app.quit", &["<primary>q"]);
    }
}

mod imp {
    use gtk::{prelude::*, subclass::prelude::*, traits::SettingsExt};
    use once_cell::unsync::OnceCell;
    use std::{cell::RefCell, collections::HashMap, rc::Rc};

    #[derive(Default, Debug)]
    pub(super) struct AppInner {
        pub(super) actions: HashMap<String, gio::Action>,
    }

    #[derive(Default)]
    pub struct App {
        pub(super) inner: OnceCell<Rc<RefCell<AppInner>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for App {
        const NAME: &'static str = "App";
        type Type = super::App;
        type ParentType = gtk::Application;
    }

    impl ObjectImpl for App {
        fn constructed(&self, obj: &Self::Type) {
            let action = gio::SimpleAction::new("app.present", None);
            action.connect_activate(glib::clone!(@weak obj => move |_, _| {
                if let Some(window) = super::main_window() {
                    window.present_with_time(unsafe { glib_sys::g_get_monotonic_time() } as u32);
                }
            }));

            obj.add_action(&action);

            self.inner
                .set(Rc::new(RefCell::new(AppInner::default())))
                .unwrap();
        }
    }

    impl ActionMapImpl for App {
        fn lookup_action(&self, _: &Self::Type, name: &str) -> Option<gio::Action> {
            let inner = self.inner.get().unwrap();
            inner.borrow().actions.get(name).map(Clone::clone)
        }

        fn add_action(&self, _: &Self::Type, action: &gio::Action) {
            let inner = self.inner.get().unwrap();
            inner
                .borrow_mut()
                .actions
                .insert(action.name().into(), action.clone());
        }

        fn remove_action(&self, _: &Self::Type, name: &str) {
            let inner = self.inner.get().unwrap();
            inner.borrow_mut().actions.remove(name);
        }
    }

    impl ApplicationImpl for App {
        fn activate(&self, obj: &Self::Type) {
            upgrade_elementary_style();

            let main_window = if let Some(main_window) = super::main_window() {
                main_window
            } else {
                let main_window = crate::main_window::MainWindow::new();
                main_window.build_ui();
                obj.setup_actions(&main_window);
                obj.add_window(&main_window);
                main_window
            };

            main_window.present();
        }
    }

    impl GtkApplicationImpl for App {}

    fn upgrade_elementary_style() {
        let stylesheet_prefix = "io.elementary.stylesheet";

        if let Some(settings) = gtk::Settings::default() {
            if settings
                .gtk_theme_name()
                .map(|name| name == "elementary")
                .unwrap_or(true)
            {
                settings.set_gtk_cursor_theme_name(Some("elementary"));
                settings.set_gtk_icon_theme_name(Some("elementary"));
                settings.set_gtk_theme_name(Some(&vec![stylesheet_prefix, "grape"].join(".")));
            }
        }
    }

    impl std::fmt::Debug for App {
        fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
            write!(f, "App")
        }
    }
}
