use crate::{daemon::DeckState, views::DeckStack, widgets::DeckItem};
use gtk::{prelude::*, subclass::prelude::*};

glib::wrapper! {
    pub struct DeckList(ObjectSubclass<imp::DeckList>)
        @extends gtk::Box, gtk::Container, gtk::Widget;
}

impl DeckList {
    pub(crate) fn new(stack: &DeckStack) -> Self {
        let this: Self = glib::Object::new(&[]).expect("Failed to create decklist");
        this.set_stack(stack);
        this
    }

    pub(crate) fn add_deck(&self, deck_state: DeckState) {
        let this = imp::DeckList::from_instance(self);
        let list = this.list_box.get().unwrap();
        let stack = this.stack.get().unwrap();

        let row = DeckItem::new(deck_state.clone());
        list.add(&row);
        stack.add_deck(deck_state);
    }

    pub(crate) fn remove_deck(&self, serial_number: String) {
        let this = imp::DeckList::from_instance(self);
        let list = this.list_box.get().unwrap();
        let stack = this.stack.get().unwrap();

        for child in list.children() {
            if let Ok(deck_item) = child.downcast::<DeckItem>() {
                if deck_item.serial_number().unwrap() == serial_number {
                    list.remove(&deck_item);
                }
            }
        }

        stack.remove_deck(&serial_number);
    }

    pub(crate) fn any_selected(&self) -> bool {
        let this = imp::DeckList::from_instance(self);
        let list = this.list_box.get().unwrap();

        list.selected_row().is_some()
    }

    pub(crate) fn select_first_item(&self) {
        let this = imp::DeckList::from_instance(self);
        let list = this.list_box.get().unwrap();

        if let Some(row) = list.row_at_index(0) {
            list.select_row(Some(&row));
        }
    }

    fn set_active(&self, row: gtk::ListBoxRow) {
        let this = imp::DeckList::from_instance(self);
        let deck_stack = this.stack.get().unwrap();

        row.activate();

        if let Ok(deck_item) = row.downcast::<DeckItem>() {
            if let Some(serial_number) = deck_item.serial_number() {
                deck_stack.select_deck(&serial_number);
            }
        }

        self.show_all();
    }

    fn set_stack(&self, stack: &DeckStack) {
        let this = imp::DeckList::from_instance(self);
        this.stack.set(stack.clone()).unwrap();
    }
}

mod imp {
    use crate::views::DeckStack;
    use gtk::{prelude::*, subclass::prelude::*};
    use once_cell::unsync::OnceCell;

    #[derive(Debug, Default)]
    pub struct DeckList {
        pub(super) stack: OnceCell<DeckStack>,
        pub(super) list_box: OnceCell<gtk::ListBox>,
        pub(super) usb_label: OnceCell<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DeckList {
        const NAME: &'static str = "DeckList";
        type Type = super::DeckList;
        type ParentType = gtk::Box;
    }

    impl ObjectImpl for DeckList {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            let alert = marble::widgets::AlertView::new(
                Some("No streamdecks found"),
                Some("Try plugging one in!"),
                None,
            );

            let list_box = gtk::ListBox::new();
            list_box.set_selection_mode(gtk::SelectionMode::Single);
            list_box.set_activate_on_single_click(true);
            list_box.set_placeholder(Some(&alert));

            let usb_label = gtk::Label::new(Some("Usb"));
            usb_label
                .style_context()
                .add_class(marble::STYLE_CLASS_H4_LABEL);
            usb_label.set_halign(gtk::Align::Start);

            list_box.set_header_func(Some(Box::new(
                glib::clone!(@weak usb_label => move |row, prev| {
                    if prev.is_some() {
                        row.set_header(None as Option<&gtk::Widget>);
                        return;
                    }

                    if usb_label.parent().is_some() {
                        usb_label.unparent();
                    }

                    row.set_header(Some(&usb_label));
                }),
            )));

            let scroll = gtk::ScrolledWindow::builder()
                .hexpand(true)
                .hscrollbar_policy(gtk::PolicyType::Never)
                .build();
            scroll.add(&list_box);

            let frame = gtk::Frame::new(None);
            frame.add(&scroll);

            obj.add(&frame);
            obj.show_all();

            list_box.connect_row_selected(glib::clone!(@weak obj => move |_list, row| {
                if let Some(row) = row {
                    obj.set_active(row.clone());
                }
            }));

            // TODO connect daemon deck name callback

            // TODO: daemon decks added callback
            // TODO: daemon decks removed callback

            self.usb_label.set(usb_label).unwrap();
            self.list_box.set(list_box).unwrap();
        }
    }

    impl WidgetImpl for DeckList {}
    impl ContainerImpl for DeckList {}
    impl BoxImpl for DeckList {}
}
