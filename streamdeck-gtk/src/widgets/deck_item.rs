use crate::daemon::DeckState;
use futures_util::stream::StreamExt;
use glib::object::ObjectExt;
use gtk::{prelude::*, subclass::prelude::*};
use marble::widgets::Dialog;

glib::wrapper! {
    pub struct DeckItem(ObjectSubclass<imp::DeckItem>)
        @extends gtk::ListBoxRow, gtk::Bin, gtk::Container, gtk::Widget;
}

impl DeckItem {
    pub(crate) fn new(deck_state: DeckState) -> Self {
        let this: Self = glib::Object::new(&[
            ("serial-number", &deck_state.serial_number()),
            ("device-name", &deck_state.device_name()),
            ("port-name", &deck_state.port_name()),
        ])
        .expect("Failed to create DeckItem");

        let stream = deck_state.device_name_stream();
        let weak = this.downgrade();
        glib::MainContext::default().spawn_local(async move {
            futures_util::pin_mut!(stream);

            while let Some(name) = stream.next().await {
                if let Some(this) = weak.upgrade() {
                    let _ = this.set_property("device-name", name);
                } else {
                    break;
                }
            }
        });

        this
    }

    pub(crate) fn serial_number(&self) -> Option<String> {
        self.try_property("serial-number").ok()
    }

    pub(crate) fn device_name(&self) -> Option<String> {
        self.try_property("device-name").ok()
    }

    fn handle_clicked(&self) -> Option<()> {
        let this = imp::DeckItem::from_instance(self);

        let mut dialog_opt = this.dialog.borrow_mut();

        let dialog: Dialog = if let Some(dialog) = &*dialog_opt {
            dialog.clone()
        } else {
            let dialog = crate::dialogs::edit_deck_dialog::build(self)?;

            dialog.set_transient_for(
                self.toplevel()
                    .and_then(|widget| widget.downcast::<crate::main_window::MainWindow>().ok())
                    .as_ref(),
            );

            *dialog_opt = Some(dialog.clone());

            dialog.connect_response(glib::clone!(@weak self as obj => move |_, _| {
                let this = imp::DeckItem::from_instance(&obj);
                let mut dialog_opt = this.dialog.borrow_mut();
                if let Some(dialog) = dialog_opt.take() {
                    dialog.close();
                }
            }));

            dialog
        };

        dialog.present();

        Some(())
    }
}

mod imp {
    use gtk::{prelude::*, subclass::prelude::*};
    use marble::widgets::Dialog;
    use once_cell::{sync::Lazy, unsync::OnceCell};
    use std::{cell::RefCell, rc::Rc};

    #[derive(Debug, Default)]
    pub struct DeckItem {
        serial_number: OnceCell<String>,
        device_name: OnceCell<Rc<RefCell<String>>>,
        port_name: OnceCell<String>,
        pub(super) dialog: RefCell<Option<Dialog>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DeckItem {
        const NAME: &'static str = "DeckItem";
        type Type = super::DeckItem;
        type ParentType = gtk::ListBoxRow;
    }

    impl ObjectImpl for DeckItem {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            let row_title = gtk::Label::new(None);
            row_title
                .style_context()
                .add_class(marble::STYLE_CLASS_H3_LABEL);
            row_title.set_halign(gtk::Align::Start);
            row_title.set_valign(gtk::Align::Start);
            row_title.set_ellipsize(pango::EllipsizeMode::End);

            let row_desc = gtk::Label::new(None);
            row_desc.set_margin_top(2);
            row_desc.set_use_markup(true);
            row_desc.set_halign(gtk::Align::Start);
            row_desc.set_valign(gtk::Align::Start);
            row_desc.set_ellipsize(pango::EllipsizeMode::End);

            let edit_button =
                gtk::Button::from_icon_name(Some("document-edit"), gtk::IconSize::Button);
            edit_button.set_tooltip_text(Some("Rename"));
            edit_button.set_halign(gtk::Align::End);
            edit_button.set_valign(gtk::Align::End);
            edit_button.set_expand(true);

            let grid = gtk::Grid::new();
            grid.set_margin(6);
            grid.set_margin_start(3);
            grid.set_column_spacing(3);

            grid.attach(&row_title, 0, 0, 2, 1);
            grid.attach(&row_desc, 0, 1, 2, 1);
            grid.attach(&edit_button, 1, 0, 1, 2);

            obj.add(&grid);
            obj.show_all();

            edit_button.connect_clicked(glib::clone!(@weak obj => move |_| {
                obj.handle_clicked();
            }));

            obj.bind_property("device-name", &row_title, "label")
                .build();
            obj.bind_property("port-name", &row_desc, "label").build();
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![
                    glib::ParamSpecString::new(
                        "serial-number",
                        "Serial Number",
                        "The serial number of the streamdeck",
                        None,
                        glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                    ),
                    glib::ParamSpecString::new(
                        "device-name",
                        "Device Name",
                        "The name of the streamdeck",
                        None,
                        glib::ParamFlags::READWRITE,
                    ),
                    glib::ParamSpecString::new(
                        "port-name",
                        "Port name",
                        "The name of the streamdecks' serial port",
                        None,
                        glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                    ),
                ]
            });

            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            _obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "device-name" => {
                    let device_name = value.get().unwrap();
                    let name = self.device_name.get();

                    if let Some(name) = name {
                        *name.borrow_mut() = device_name;
                    } else {
                        self.device_name
                            .set(Rc::new(RefCell::new(device_name)))
                            .unwrap();
                    }
                }
                "serial-number" => {
                    let serial_number = value.get().unwrap();
                    self.serial_number.set(serial_number).unwrap();
                }
                "port-name" => {
                    let port_name = value.get().unwrap();
                    self.port_name.set(port_name).unwrap();
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "serial-number" => self.serial_number.get().unwrap().to_value(),
                "device-name" => self.device_name.get().unwrap().borrow().to_value(),
                "port-name" => self.port_name.get().unwrap().to_value(),
                _ => unimplemented!(),
            }
        }
    }

    impl WidgetImpl for DeckItem {}
    impl ContainerImpl for DeckItem {}
    impl BinImpl for DeckItem {}
    impl ListBoxRowImpl for DeckItem {}
}
