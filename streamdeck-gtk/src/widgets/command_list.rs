use crate::{
    daemon::{CommandState, DeckState, Handle},
    widgets::CommandRow,
};
use futures_util::stream::StreamExt;
use gtk::{prelude::*, subclass::prelude::*};
use marble::widgets::Dialog;
use streamdeck_common::Input;

glib::wrapper! {
    pub struct CommandList(ObjectSubclass<imp::CommandList>)
        @extends gtk::Box, gtk::Container, gtk::Widget;
}

impl CommandList {
    pub(crate) fn new(deck_state: DeckState) -> Self {
        let this: Self = glib::Object::new(&[]).expect("Failed to create command list");

        let serial_number = deck_state.serial_number();
        let inner = imp::CommandList::from_instance(&this);
        inner.serial_number.set(serial_number).unwrap();

        for command in deck_state.commands() {
            this.add_command(command);
        }

        let stream = deck_state.clone().added_command_stream();
        let weak = this.downgrade();
        glib::MainContext::default().spawn_local(async move {
            futures_util::pin_mut!(stream);

            while let Some(command_state) = stream.next().await {
                if let Some(this) = weak.upgrade() {
                    this.add_command(command_state);
                } else {
                    break;
                }
            }
        });

        let stream = deck_state.removed_command_stream();
        let weak = this.downgrade();
        glib::MainContext::default().spawn_local(async move {
            futures_util::pin_mut!(stream);

            while let Some(keys) = stream.next().await {
                if let Some(this) = weak.upgrade() {
                    this.remove_command(keys);
                } else {
                    break;
                }
            }
        });

        this
    }

    fn add_command(&self, command: CommandState) {
        let inner = imp::CommandList::from_instance(self);
        let list_box = inner.list_box.get().unwrap();
        let serial_number = inner.serial_number.get().unwrap();
        let mut rows = inner.rows.borrow_mut();

        if rows.contains_key(&command.keys()) {
            return;
        }

        let keys = command.keys();
        let row = CommandRow::new(serial_number, command);

        list_box.add(&row);
        rows.insert(keys, row);
    }

    fn remove_command(&self, keys: Input) {
        let inner = imp::CommandList::from_instance(self);
        let mut rows = inner.rows.borrow_mut();

        if let Some(row) = rows.remove(&keys) {
            let list_box = inner.list_box.get().unwrap();
            list_box.remove(&row);
        }
    }

    fn on_remove_click(&self) -> Option<()> {
        let this = imp::CommandList::from_instance(self);
        let list_box = this.list_box.get().unwrap();

        let row = list_box.selected_row()?;
        let command_row = row.downcast::<CommandRow>().ok()?;

        let serial_number = command_row.serial_number();
        let input_keys = command_row.input_keys();

        glib::MainContext::default().spawn_local(
            glib::clone!(@weak self as obj, @weak command_row => async move {
                let this = imp::CommandList::from_instance(&obj);
                let list_box = this.list_box.get().unwrap();

                if Handle::current().unset_input(serial_number, input_keys.clone()).await.is_ok() {
                    list_box.remove(&command_row);
                    this.rows.borrow_mut().remove(&input_keys);
                }

                obj.show_all();
            }),
        );

        Some(())
    }

    fn on_add_click(&self) {
        let this = imp::CommandList::from_instance(self);

        let serial_number = this.serial_number.get().unwrap();
        let mut dialog_opt = this.dialog.borrow_mut();

        let dialog: Dialog = if let Some(dialog) = &*dialog_opt {
            dialog.clone()
        } else {
            let dialog = crate::dialogs::new_command_dialog::build(serial_number.to_owned());

            dialog.set_transient_for(
                self.toplevel()
                    .and_then(|widget| widget.downcast::<crate::main_window::MainWindow>().ok())
                    .as_ref(),
            );

            *dialog_opt = Some(dialog.clone());

            dialog.connect_response(glib::clone!(@weak self as obj => move |_, response_type| {
                if response_type == gtk::ResponseType::Reject {
                    if let Some(main_window) = obj.toplevel().and_then(|widget| widget.downcast::<crate::main_window::MainWindow>().ok()) {
                        main_window.to_obs()
                    }
                }

                let this = imp::CommandList::from_instance(&obj);
                let mut dialog_opt = this.dialog.borrow_mut();
                if let Some(dialog) = dialog_opt.take() {
                    dialog.close();
                }
            }));

            dialog
        };

        dialog.present();
    }
}

mod imp {
    use crate::widgets::CommandRow;
    use gtk::{prelude::*, subclass::prelude::*};
    use marble::widgets::{AlertView, Dialog};
    use once_cell::unsync::OnceCell;
    use std::{cell::RefCell, collections::HashMap};
    use streamdeck_common::Input;

    #[derive(Debug, Default)]
    pub struct CommandList {
        pub(super) serial_number: OnceCell<String>,
        pub(super) list_box: OnceCell<gtk::ListBox>,
        pub(super) rows: RefCell<HashMap<Input, CommandRow>>,
        pub(super) dialog: RefCell<Option<Dialog>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CommandList {
        const NAME: &'static str = "CommandList";
        type Type = super::CommandList;
        type ParentType = gtk::Box;
    }

    impl ObjectImpl for CommandList {
        fn constructed(&self, obj: &Self::Type) {
            let alert = AlertView::new(
                Some("No commands registered"),
                Some("Try adding a new command."),
                None,
            );

            let list_box = gtk::ListBox::new();
            list_box.set_selection_mode(gtk::SelectionMode::Single);
            list_box.set_activate_on_single_click(true);
            list_box.set_placeholder(Some(&alert));

            let add_button =
                gtk::Button::from_icon_name(Some("list-add-symbolic"), gtk::IconSize::Button);
            add_button.set_tooltip_text(Some("Add"));

            let remove_button =
                gtk::Button::from_icon_name(Some("list-remove-symbolic"), gtk::IconSize::Button);
            remove_button.set_tooltip_text(Some("Remove"));

            let action_bar = gtk::ActionBar::new();
            action_bar
                .style_context()
                .add_class(&gtk::STYLE_CLASS_INLINE_TOOLBAR);
            action_bar.add(&add_button);
            action_bar.add(&remove_button);
            action_bar.show_all();

            let scrolled = gtk::ScrolledWindow::builder().expand(true).build();
            scrolled.add(&list_box);

            let grid = gtk::Grid::new();
            grid.attach(&scrolled, 0, 0, 1, 1);
            grid.attach(&action_bar, 0, 1, 1, 1);

            let frame = gtk::Frame::new(None);
            frame.add(&grid);

            obj.set_expand(true);
            obj.add(&frame);
            obj.show_all();

            remove_button.connect_clicked(glib::clone!(@weak obj => move |_| {
                let _ = obj.on_remove_click();
            }));

            add_button.connect_clicked(glib::clone!(@weak obj => move |_| {
                obj.on_add_click();
            }));

            self.list_box.set(list_box).unwrap();
        }
    }
    impl WidgetImpl for CommandList {}
    impl ContainerImpl for CommandList {}
    impl BoxImpl for CommandList {}
}
