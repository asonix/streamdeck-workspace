use gtk::prelude::*;

pub(crate) fn disconnected_page() -> gtk::Grid {
    let title_label = gtk::Label::new(Some("OBS is currently disconnected"));
    title_label
        .style_context()
        .add_class(marble::STYLE_CLASS_H2_LABEL);
    title_label.set_hexpand(true);
    title_label.set_max_width_chars(75);
    title_label.set_wrap(true);
    title_label.set_wrap_mode(pango::WrapMode::WordChar);
    title_label.set_xalign(0f32);

    let description_label = gtk::Label::new(Some("Try connecting it before configuring commands"));
    description_label.set_hexpand(true);
    description_label.set_max_width_chars(75);
    description_label.set_wrap(true);
    description_label.set_use_markup(true);
    description_label.set_xalign(0f32);
    description_label.set_valign(gtk::Align::Start);

    let grid = gtk::Grid::new();
    grid.set_column_spacing(12);
    grid.set_row_spacing(6);
    grid.set_halign(gtk::Align::Center);
    grid.set_valign(gtk::Align::Center);
    grid.set_vexpand(true);
    grid.set_margin(24);

    grid.attach(&title_label, 1, 1, 1, 1);
    grid.attach(&description_label, 1, 2, 1, 1);
    grid.show_all();

    grid
}
