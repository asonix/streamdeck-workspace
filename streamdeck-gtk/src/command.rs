use crate::daemon::Handle;
use event_listener::{Event, EventListener};
use futures_core::stream::Stream;
use glib::object::WeakRef;
use gtk::prelude::*;
use std::{
    future::Future,
    pin::Pin,
    rc::{Rc, Weak},
    task::{Context, Poll},
};
use streamdeck_common::{Command, CommandVariant, StateOperation, VisibilityOperation};

#[derive(Debug)]
pub(crate) struct CommandRowState {
    row_stack: WeakRef<gtk::Stack>,
    switch_scene: SwitchSceneState,
    set_scene_item_visibility: SetSceneItemVisibilityState,
    set_recording: SetRecordingState,
    set_streaming: SetStreamingState,
}

#[derive(Debug)]
struct SwitchSceneState {
    scene_name: WeakRef<gtk::Label>,
}

#[derive(Debug)]
struct SetSceneItemVisibilityState {
    scene_name: WeakRef<gtk::Label>,
    source_name: WeakRef<gtk::Label>,
    operation: WeakRef<gtk::Label>,
}

#[derive(Debug)]
struct SetRecordingState {
    operation: WeakRef<gtk::Label>,
}

#[derive(Debug)]
struct SetStreamingState {
    operation: WeakRef<gtk::Label>,
}

impl CommandRowState {
    pub(crate) fn build(command: &Command, grid: &gtk::Grid) -> Self {
        let scene_name = styled_label(None);
        let scene_grid = render_switch_scene(&scene_name);

        let visibility_scene_name = styled_label(None);
        let visibility_source_name = styled_label(None);
        let visibility_operation = styled_label(None);
        let visibility_grid = render_set_scene_item_visibility(
            &visibility_scene_name,
            &visibility_source_name,
            &visibility_operation,
        );

        let recording_operation = styled_label(None);
        let recording_grid = render_set_recording(&recording_operation);

        let streaming_operation = styled_label(None);
        let streaming_grid = render_set_streaming(&streaming_operation);

        let row_stack = gtk::Stack::new();
        row_stack.add_named(&scene_grid, CommandVariant::SwitchScene.as_id());
        row_stack.add_named(
            &visibility_grid,
            CommandVariant::SetSceneItemVisibility.as_id(),
        );
        row_stack.add_named(&recording_grid, CommandVariant::SetRecording.as_id());
        row_stack.add_named(&streaming_grid, CommandVariant::SetStreaming.as_id());
        row_stack.show_all();

        grid.add(&row_stack);

        let this = CommandRowState {
            row_stack: row_stack.downgrade(),
            switch_scene: SwitchSceneState {
                scene_name: scene_name.downgrade(),
            },
            set_scene_item_visibility: SetSceneItemVisibilityState {
                scene_name: visibility_scene_name.downgrade(),
                source_name: visibility_source_name.downgrade(),
                operation: visibility_operation.downgrade(),
            },
            set_recording: SetRecordingState {
                operation: recording_operation.downgrade(),
            },
            set_streaming: SetStreamingState {
                operation: streaming_operation.downgrade(),
            },
        };

        this.update_command(command);

        this
    }

    pub(crate) fn update_command(&self, command: &Command) -> Option<()> {
        match command {
            Command::SwitchScene { to } => {
                let label = self.switch_scene.scene_name.upgrade()?;
                label.set_label(to);
                label.show_all();
            }
            Command::SetSceneItemVisibility {
                scene_name,
                item_id,
                operation,
            } => {
                let scene_label = self.set_scene_item_visibility.scene_name.upgrade()?;
                scene_label.set_label(scene_name);
                scene_label.show_all();

                let source_label = self.set_scene_item_visibility.source_name.clone();

                let scene_name = scene_name.clone();
                let item_id = *item_id;
                glib::MainContext::default().spawn_local(async move {
                    if let Some(source_label) = source_label.upgrade() {
                        if let Ok(item) =
                            Handle::current().get_scene_item(scene_name, item_id).await
                        {
                            let source_name = item.name;
                            source_label.set_label(&source_name);
                        }
                    }
                });

                let op_label = self.set_scene_item_visibility.operation.upgrade()?;
                match operation {
                    VisibilityOperation::On => {
                        op_label.set_label("Show");
                    }
                    VisibilityOperation::Off => {
                        op_label.set_label("Hide");
                    }
                    VisibilityOperation::Toggle => {
                        op_label.set_label("Toggle Visibility for");
                    }
                }
            }
            Command::SetRecording { operation } => {
                let op_label = self.set_recording.operation.upgrade()?;
                match operation {
                    StateOperation::Start => {
                        op_label.set_label("Start");
                    }
                    StateOperation::Stop => {
                        op_label.set_label("Stop");
                    }
                    StateOperation::Toggle => {
                        op_label.set_label("Toggle");
                    }
                }
            }
            Command::SetStreaming { operation } => {
                let op_label = self.set_streaming.operation.upgrade()?;
                match operation {
                    StateOperation::Start => {
                        op_label.set_label("Start");
                    }
                    StateOperation::Stop => {
                        op_label.set_label("Stop");
                    }
                    StateOperation::Toggle => {
                        op_label.set_label("Toggle");
                    }
                }
            }
        }

        let cmd_type = command.to_variant();
        let row_stack = self.row_stack.upgrade()?;
        row_stack.set_visible_child_name(cmd_type.as_id());

        Some(())
    }
}

fn grid_builder() -> gtk::Grid {
    let grid = gtk::Grid::new();
    grid.set_halign(gtk::Align::Start);
    grid.set_valign(gtk::Align::Center);
    grid.set_orientation(gtk::Orientation::Horizontal);
    grid.set_margin(0);
    grid.set_margin_start(3);
    grid.set_column_spacing(6);
    grid
}

fn styled_label(label: Option<&str>) -> gtk::Label {
    let label = gtk::Label::new(label);
    label.set_halign(gtk::Align::Start);
    label.set_valign(gtk::Align::Center);
    label
}

fn render_set_recording(operation: &gtk::Label) -> gtk::Grid {
    let recording_label = styled_label(Some("streaming"));

    // [Start]  Recording
    // [Stop]   Recording
    // [Toggle] Recording
    let grid = grid_builder();
    grid.add(operation);
    grid.add(&recording_label);
    grid
}

fn render_set_streaming(operation: &gtk::Label) -> gtk::Grid {
    let streaming_label = styled_label(Some("streaming"));

    // [Start]  Streaming
    // [Stop]   Streaming
    // [Toggle] Streaming
    let grid = grid_builder();
    grid.add(operation);
    grid.add(&streaming_label);
    grid
}

fn render_set_scene_item_visibility(
    scene_name: &gtk::Label,
    source_name: &gtk::Label,
    operation: &gtk::Label,
) -> gtk::Grid {
    let in_label = styled_label(Some("in"));

    // [Show]                  [Source Name] in [Scene Name]
    // [Hide]                  [Source Name] in [Scene Name]
    // [Toggle Visibility for] [Source Name] in [Scene Name]
    let grid = grid_builder();
    grid.add(operation);
    grid.add(source_name);
    grid.add(&in_label);
    grid.add(scene_name);
    grid
}

fn render_switch_scene(scene_name: &gtk::Label) -> gtk::Grid {
    let switch_scene_to = styled_label(Some("Switch scene to"));

    // Switch scene to [Scene Name]
    let grid = grid_builder();
    grid.add(&switch_scene_to);
    grid.add(scene_name);
    grid.show_all();
    grid
}

pub(crate) fn render_config(command: Option<&Command>) -> (gtk::ComboBoxText, ComboBoxHandle) {
    let combobox = gtk::ComboBoxText::new();
    combobox.set_id_column(1);

    for typ in CommandVariant::all() {
        combobox.append(Some(typ.as_id()), &to_name(*typ));
    }

    let event = Rc::new(Event::default());

    let handle = ComboBoxHandle {
        obj: combobox.downgrade(),
        event: Rc::downgrade(&event),
    };

    let command_type = command.as_ref().map(|c| c.to_variant()).unwrap_or_default();

    combobox.set_active_id(Some(command_type.as_id()));

    combobox.connect_changed(move |_combobox| {
        event.notify(usize::MAX);
    });

    (combobox, handle)
}

#[derive(Clone, Debug)]
pub(crate) struct ComboBoxHandle {
    obj: WeakRef<gtk::ComboBoxText>,
    event: Weak<Event>,
}

pub(crate) struct SelectedStream {
    listener: Option<EventListener>,
    event: Weak<Event>,
    obj: WeakRef<gtk::ComboBoxText>,
}

pub(crate) fn to_name(variant: CommandVariant) -> String {
    match variant {
        CommandVariant::SwitchScene => "Switch Scene".to_string(),
        CommandVariant::SetSceneItemVisibility => "Set Visibility".to_string(),
        CommandVariant::SetStreaming => "Set Streaming".to_string(),
        CommandVariant::SetRecording => "Set Recording".to_string(),
    }
}

pub(crate) async fn to_default_command(variant: CommandVariant) -> Option<Command> {
    match variant {
        CommandVariant::SwitchScene => {
            let scenes = Handle::current().get_scenes().await.ok()?;

            let first_scene = scenes.get(0)?;

            Some(Command::SwitchScene {
                to: first_scene.name.clone(),
            })
        }
        CommandVariant::SetSceneItemVisibility => {
            let scenes = Handle::current().get_scenes().await.ok()?;
            let first_scene = scenes.get(0)?;

            let items = Handle::current()
                .get_scene_items(first_scene.name.clone())
                .await
                .ok()?;
            let first_item = items.get(0)?;

            Some(Command::SetSceneItemVisibility {
                scene_name: first_scene.name.clone(),
                item_id: first_item.id,
                operation: VisibilityOperation::Toggle,
            })
        }
        CommandVariant::SetStreaming => Some(Command::SetStreaming {
            operation: StateOperation::Toggle,
        }),
        CommandVariant::SetRecording => Some(Command::SetRecording {
            operation: StateOperation::Toggle,
        }),
    }
}

impl ComboBoxHandle {
    pub(crate) fn selected_stream(&self) -> Option<SelectedStream> {
        self.event.upgrade().map(|event| SelectedStream {
            listener: Some(event.listen()),
            event: self.event.clone(),
            obj: self.obj.clone(),
        })
    }

    pub(crate) fn selected(&self) -> Option<CommandVariant> {
        self.obj.upgrade()?.active_id()?.parse().ok()
    }

    pub(crate) fn select(&self, command_type: CommandVariant) {
        if let Some(combobox) = self.obj.upgrade() {
            combobox.set_active_id(Some(command_type.as_id()));
        }
    }
}

impl Stream for SelectedStream {
    type Item = CommandVariant;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let event = if let Some(event) = self.event.upgrade() {
            event
        } else {
            return Poll::Ready(None);
        };

        let mut listener = if let Some(listener) = self.listener.take() {
            listener
        } else {
            event.listen()
        };

        match Pin::new(&mut listener).poll(cx) {
            Poll::Ready(()) => {
                let mut listener;
                while {
                    listener = event.listen();

                    Pin::new(&mut listener).poll(cx).is_ready()
                } {}

                self.listener = Some(listener);

                if let Some(combobox) = self.obj.upgrade() {
                    if let Some(selected) = combobox.active_id() {
                        if let Ok(cmd_type) = selected.parse() {
                            Poll::Ready(Some(cmd_type))
                        } else {
                            Poll::Pending
                        }
                    } else {
                        Poll::Pending
                    }
                } else {
                    Poll::Ready(None)
                }
            }

            Poll::Pending => {
                self.listener = Some(listener);
                Poll::Pending
            }
        }
    }
}
