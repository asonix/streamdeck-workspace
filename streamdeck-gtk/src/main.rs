use gio::{prelude::*, Settings};
use once_cell::unsync::Lazy;

mod application;
mod command;
mod config;
mod dbus;
mod dialogs;
mod main_window;
mod views;
mod widgets;

fn obs_settings() -> Settings {
    thread_local! {
        static OBS_SETTINGS: Lazy<Settings> =
            Lazy::new(|| Settings::new("dog.asonix.git.asonix.Streamdeck.obs"));
    }

    OBS_SETTINGS.with(|settings| (*settings).clone())
}

fn saved_state() -> Settings {
    thread_local! {
        static SAVED_STATE: Lazy<Settings> =
            Lazy::new(|| Settings::new("dog.asonix.git.asonix.Streamdeck.saved-state"));
    }

    SAVED_STATE.with(|settings| (*settings).clone())
}

fn main() -> anyhow::Result<()> {
    init_tracing()?;
    gtk::init()?;
    libhandy::functions::init();

    let handle = daemon::Handle::current();
    glib::MainContext::default().spawn(daemon::state_management());

    application::App::new().run_with_args(&std::env::args().collect::<Vec<_>>());
    drop(handle);
    Ok(())
}

fn init_tracing() -> anyhow::Result<()> {
    use tracing::subscriber::set_global_default;
    use tracing_error::ErrorLayer;
    use tracing_log::LogTracer;
    use tracing_subscriber::{
        filter::Targets, fmt::format::FmtSpan, layer::SubscriberExt, Layer, Registry,
    };

    LogTracer::init()?;

    let targets = std::env::var("RUST_LOG")
        .unwrap_or_else(|_| "streamdeck_daemon=debug,streamdeck_handshake=debug,info".into())
        .parse::<Targets>()?;

    let format_layer = tracing_subscriber::fmt::layer()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_filter(targets);

    let subscriber = Registry::default()
        .with(format_layer)
        .with(ErrorLayer::default());

    set_global_default(subscriber)?;

    Ok(())
}
