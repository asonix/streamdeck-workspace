use streamdeck_common::Input;

#[zbus::dbus_proxy(
    interface = "dog.asonix.git.asonix.StreamdeckDaemon",
    default_service = "dog.asonix.git.asonix.StreamdeckDaemon",
    default_path = "/dog/asonix/git/asonix/StreamdeckDaemon"
)]
trait Daemon {
    #[dbus_proxy(property)]
    fn discovery_enabled(&self) -> zbus::fdo::Result<bool>;

    #[dbus_proxy(property)]
    fn set_discovery_enabled(&self, enabled: bool) -> zbus::fdo::Result<()>;

    fn get_decks(&self) -> zbus::fdo::Result<Vec<String>>;

    #[dbus_proxy(signal)]
    fn deck_added(&self, path: &str) -> zbus::fdo::Result<()>;

    #[dbus_proxy(signal)]
    fn deck_removed(&self, path: &str) -> zbus::fdo::Result<()>;
}

#[zbus::dbus_proxy(
    interface = "dog.asonix.git.asonix.StreamdeckDaemon.Deck",
    default_service = "dog.asonix.git.asonix.StreamdeckDaemon.Deck"
)]
trait Deck {
    #[dbus_proxy(property)]
    fn name(&self) -> zbus::fdo::Result<String>;

    #[dbus_proxy(property)]
    fn set_name(&self, name: String) -> zbus::fdo::Result<()>;

    #[dbus_proxy(property)]
    fn product_name(&self) -> zbus::fdo::Result<String>;

    #[dbus_proxy(property)]
    fn port_name(&self) -> zbus::fdo::Result<String>;

    fn get_buttons(&self) -> zbus::fdo::Result<Vec<String>>;

    fn create_button(&self, input: Input, command: String) -> zbus::fdo::Result<String>;

    fn remove_button(&self, input: Input) -> zbus::fdo::Result<()>;

    #[dbus_proxy(signal)]
    fn button_pushed(&self, input: Input) -> zbus::fdo::Result<()>;

    #[dbus_proxy(signal)]
    fn button_added(&self, input: Input) -> zbus::fdo::Result<()>;

    #[dbus_proxy(signal)]
    fn button_removed(&self, input: Input) -> zbus::fdo::Result<()>;
}

#[zbus::dbus_proxy(
    interface = "dog.asonix.git.asonix.StreamdeckDaemon.Deck.Button",
    default_service = "dog.asonix.git.asonix.StreamdeckDaemon.Deck.Button"
)]
trait Button {
    #[dbus_proxy(property)]
    fn name(&self) -> zbus::fdo::Result<String>;

    #[dbus_proxy(property)]
    fn set_name(&self, name: String) -> zbus::fdo::Result<()>;

    #[dbus_proxy(property)]
    fn command(&self) -> zbus::fdo::Result<String>;

    #[dbus_proxy(property)]
    fn set_command(&self, command: String) -> zbus::fdo::Result<()>;

    #[dbus_proxy(signal)]
    fn pushed(&self) -> zbus::fdo::Result<()>;
}
