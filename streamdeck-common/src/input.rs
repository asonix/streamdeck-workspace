use std::ops::Deref;

#[derive(
    Clone,
    Debug,
    Default,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    serde::Deserialize,
    serde::Serialize,
)]
#[cfg_attr(feature = "zbus", derive(zbus::zvariant::Type))]
#[serde(transparent)]
pub struct Input {
    keys: Vec<u8>,
}

impl Input {
    pub fn from_slice(slice: &[u8]) -> Self {
        Input {
            keys: slice.to_vec(),
        }
    }

    pub fn from_vec(keys: Vec<u8>) -> Self {
        Input { keys }
    }
}

impl Deref for Input {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        &self.keys
    }
}

impl AsRef<[u8]> for Input {
    fn as_ref(&self) -> &[u8] {
        &self.keys
    }
}

impl From<&[u8]> for Input {
    fn from(slice: &[u8]) -> Self {
        Self::from_slice(slice)
    }
}

impl From<Vec<u8>> for Input {
    fn from(vec: Vec<u8>) -> Self {
        Self::from_vec(vec)
    }
}

impl std::fmt::Display for Input {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let pressed = self
            .keys
            .iter()
            .map(|key| key.to_string())
            .collect::<Vec<_>>()
            .join(", ");
        write!(f, "Pressed {{ {} }}", pressed)
    }
}
