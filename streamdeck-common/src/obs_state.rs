use crate::ParseErr;

#[derive(
    Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, serde::Deserialize, serde::Serialize,
)]
pub enum ObsState {
    Disconnected,
    Unauthenticated,
    Connected,
}

impl ObsState {
    pub fn to_str(&self) -> &'static str {
        match self {
            Self::Disconnected => "Disconnected",
            Self::Unauthenticated => "Unauthenticated",
            Self::Connected => "Connected",
        }
    }

    pub fn is_connected(&self) -> bool {
        matches!(self, Self::Connected)
    }

    pub fn is_unauthenticated(&self) -> bool {
        matches!(self, Self::Unauthenticated)
    }

    pub fn is_disconnected(&self) -> bool {
        matches!(self, Self::Disconnected)
    }
}

impl std::str::FromStr for ObsState {
    type Err = ParseErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Disconnected" => Ok(Self::Disconnected),
            "Unauthenticated" => Ok(Self::Unauthenticated),
            "Connected" => Ok(Self::Connected),
            _ => Err(ParseErr(s.to_string())),
        }
    }
}

impl std::fmt::Display for ObsState {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.to_str())
    }
}

impl Default for ObsState {
    fn default() -> Self {
        ObsState::Disconnected
    }
}
