mod command;
mod input;
mod obs_state;
mod query;

pub use command::{Command, CommandVariant, StateOperation, VisibilityOperation};
pub use input::Input;
pub use obs_state::ObsState;
pub use query::{Query, QueryResponse, Scene, SceneItem};

#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct ParseErr(String);

impl std::fmt::Display for ParseErr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Invalid state '{}' supplied", self.0)
    }
}

impl std::error::Error for ParseErr {}
