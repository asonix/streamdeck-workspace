use crate::ParseErr;

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(tag = "type")]
pub enum Command {
    SwitchScene {
        to: String,
    },
    SetSceneItemVisibility {
        scene_name: String,
        item_id: i64,
        operation: VisibilityOperation,
    },
    SetStreaming {
        operation: StateOperation,
    },
    SetRecording {
        operation: StateOperation,
    },
}

#[derive(
    Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, serde::Deserialize, serde::Serialize,
)]
pub enum VisibilityOperation {
    On,
    Off,
    Toggle,
}

#[derive(
    Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, serde::Deserialize, serde::Serialize,
)]
pub enum StateOperation {
    Start,
    Stop,
    Toggle,
}

#[derive(
    Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, serde::Deserialize, serde::Serialize,
)]
pub enum CommandVariant {
    SwitchScene,
    SetSceneItemVisibility,
    SetStreaming,
    SetRecording,
}

impl Command {
    pub fn to_variant(&self) -> CommandVariant {
        match self {
            Self::SwitchScene { .. } => CommandVariant::SwitchScene,
            Self::SetSceneItemVisibility { .. } => CommandVariant::SetSceneItemVisibility,
            Self::SetStreaming { .. } => CommandVariant::SetStreaming,
            Self::SetRecording { .. } => CommandVariant::SetRecording,
        }
    }
}

impl StateOperation {
    pub fn as_id(&self) -> &'static str {
        match self {
            StateOperation::Start => "Start",
            StateOperation::Stop => "Stop",
            StateOperation::Toggle => "Toggle",
        }
    }

    pub fn all() -> &'static [StateOperation] {
        static ALL: &[StateOperation] = &[
            StateOperation::Start,
            StateOperation::Stop,
            StateOperation::Toggle,
        ];

        ALL
    }
}

impl VisibilityOperation {
    pub fn as_id(&self) -> &'static str {
        match self {
            VisibilityOperation::On => "Show",
            VisibilityOperation::Off => "Hide",
            VisibilityOperation::Toggle => "Toggle",
        }
    }

    pub fn all() -> &'static [VisibilityOperation] {
        static ALL: &[VisibilityOperation] = &[
            VisibilityOperation::On,
            VisibilityOperation::Off,
            VisibilityOperation::Toggle,
        ];

        ALL
    }
}

impl CommandVariant {
    pub fn all() -> &'static [Self] {
        static ALL: &[CommandVariant] = &[
            CommandVariant::SwitchScene,
            CommandVariant::SetSceneItemVisibility,
            CommandVariant::SetStreaming,
            CommandVariant::SetRecording,
        ];
        ALL
    }

    pub fn as_id(&self) -> &'static str {
        match self {
            Self::SwitchScene => "SwitchScene",
            Self::SetSceneItemVisibility => "SetSceneItemVisibility",
            Self::SetStreaming => "SetStreaming",
            Self::SetRecording => "SetRecording",
        }
    }
}

impl Default for CommandVariant {
    fn default() -> Self {
        CommandVariant::SwitchScene
    }
}

impl std::str::FromStr for VisibilityOperation {
    type Err = ParseErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Show" => Ok(VisibilityOperation::On),
            "Hide" => Ok(VisibilityOperation::Off),
            "Toggle" => Ok(VisibilityOperation::Toggle),
            _ => Err(ParseErr(s.to_owned())),
        }
    }
}

impl std::str::FromStr for StateOperation {
    type Err = ParseErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Start" => Ok(StateOperation::Start),
            "Stop" => Ok(StateOperation::Stop),
            "Toggle" => Ok(StateOperation::Toggle),
            _ => Err(ParseErr(s.to_owned())),
        }
    }
}

impl std::str::FromStr for CommandVariant {
    type Err = ParseErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "SwitchScene" => Ok(Self::SwitchScene),
            "SetSceneItemVisibility" => Ok(Self::SetSceneItemVisibility),
            "SetStreaming" => Ok(Self::SetStreaming),
            "SetRecording" => Ok(Self::SetRecording),
            _ => Err(ParseErr(s.to_owned())),
        }
    }
}

impl std::fmt::Display for VisibilityOperation {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.as_id())
    }
}

impl std::fmt::Display for StateOperation {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.as_id())
    }
}

impl std::fmt::Display for CommandVariant {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.as_id())
    }
}

#[cfg(test)]
mod tests {
    use super::{CommandVariant, StateOperation, VisibilityOperation};

    #[test]
    fn round_trip_state() {
        for state in StateOperation::all() {
            let new_state: StateOperation = state.as_id().parse().unwrap();
            assert_eq!(*state, new_state);
        }
    }

    #[test]
    fn round_trip_visibility() {
        for state in VisibilityOperation::all() {
            let new_state: VisibilityOperation = state.as_id().parse().unwrap();
            assert_eq!(*state, new_state);
        }
    }

    #[test]
    fn round_trip_command_variant() {
        for state in CommandVariant::all() {
            let new_state: CommandVariant = state.as_id().parse().unwrap();
            assert_eq!(*state, new_state);
        }
    }
}
