#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(tag = "type")]
pub enum Query {
    GetScenes,
    GetSceneItems { scene_name: String },
    GetSceneItem { scene_name: String, item_id: i64 },
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
#[serde(tag = "type")]
pub enum QueryResponse {
    Scenes { scenes: Vec<Scene> },
    SceneItems { items: Vec<SceneItem> },
    SceneItem { item: SceneItem },
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct Scene {
    pub name: String,
}

#[derive(Clone, Debug, serde::Deserialize, serde::Serialize)]
pub struct SceneItem {
    pub id: i64,
    pub name: String,
    pub visible: bool,
}
