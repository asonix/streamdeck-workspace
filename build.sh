#!/usr/bin/env bash

set -xe

flatpak-builder build dog.asonix.git.asonix.Streamdeck.yml --user --install --force-clean
