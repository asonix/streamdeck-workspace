use gtk::prelude::*;
use marble::widgets::Dialog;

fn main() {
    gtk::init().expect("Failed to initialize gtk");
    let dialog = Dialog::new();

    let label = gtk::Label::new(Some("Hewwo owo"));
    dialog.content_area().add(&label);
    dialog.show_all();

    dialog.run();
}
