use futures_lite::prelude::*;
use gtk::prelude::*;
use marble::widgets::{AlertView, Dialog};

fn main() {
    gtk::init().expect("Failed to initialize gtk");

    let alert_view = AlertView::new(
        Some("Hewwo"),
        Some("owo uwu owo uwu mr obama"),
        Some("document-edit"),
    );

    alert_view.show_action(Some("Henlo"));
    let mut stream = alert_view.action_activated();

    glib::MainContext::default().spawn_local(async move {
        while let Some(()) = stream.next().await {
            println!("Button clicked");
        }
    });

    let frame = gtk::Frame::new(None);
    frame.add(&alert_view);

    let dialog = Dialog::new();
    dialog.content_area().add(&frame);
    dialog.show_all();

    dialog.run();
}
