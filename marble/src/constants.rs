/**
 * Style class to give accent color to a {@link Gtk.Label} or symbolic icon
 */
pub const STYLE_CLASS_ACCENT: &str = "accent";

/**
 * Style class for shaping a {@link Gtk.Button}
 */
pub const STYLE_CLASS_BACK_BUTTON: &str = "back-button";
/**
 * Style class for numbered badges as in a {@link Granite.Widgets.SourceList}
 */
pub const STYLE_CLASS_BADGE: &str = "badge";
/**
 * Style class for adding a small shadow to a container such as for image thumbnails
 *
 * Can be combined with the style class ".collapsed" to further reduce the size of the shadow
 */
pub const STYLE_CLASS_CARD: &str = "card";
pub const STYLE_CLASS_CATEGORY_EXPANDER: &str = "category-expander";
/**
 * Style class for checkered backgrounds to represent transparency in images
 */
pub const STYLE_CLASS_CHECKERBOARD: &str = "checkerboard";
/**
 * Style class for color chooser buttons to be applied to {@link Gtk.CheckButton} or {@link Gtk.RadioButton}
 */
pub const STYLE_CLASS_COLOR_BUTTON: &str = "color-button";
/**
 * Style class for slim headerbars, like in Terminal
 */
pub const STYLE_CLASS_DEFAULT_DECORATION: &str = "default-decoration";
/**
 * Style class for large primary text as seen in {@link Granite.Widgets.Welcome}
 */
pub const STYLE_CLASS_H1_LABEL: &str = "h1";
/**
 * Style class for large seondary text as seen in {@link Granite.Widgets.Welcome}
 */
pub const STYLE_CLASS_H2_LABEL: &str = "h2";
/**
 * Style class for small primary text
 */
pub const STYLE_CLASS_H3_LABEL: &str = "h3";
/**
 * Style class for a {@link Granite.HeaderLabel}
 */
pub const STYLE_CLASS_H4_LABEL: &str = "h4";
/**
 * Style class for a {@link Gtk.Label} to be displayed as a keyboard key cap
 */
pub const STYLE_CLASS_KEYCAP: &str = "keycap";
/**
 * Style class for a {@link Gtk.Switch} used to change between two modes rather than active and inactive states
 */
pub const STYLE_CLASS_MODE_SWITCH: &str = "mode-switch";
/**
 * Style class for a {@link Granite.Widgets.OverlayBar}
 */
pub const STYLE_CLASS_OVERLAY_BAR: &str = "overlay-bar";
/**
 * Style class for primary label text in a {@link Granite.MessageDialog}
 */
pub const STYLE_CLASS_PRIMARY_LABEL: &str = "primary";
/**
 * Style class for rounded corners, i.e. on a {@link Gtk.Window} or {@link Granite.STYLE_CLASS_CARD}
 */
pub const STYLE_CLASS_ROUNDED: &str = "rounded";
/**
 * Style class for a {@link Granite.SeekBar}
 */
pub const STYLE_CLASS_SEEKBAR: &str = "seek-bar";
/**
 * Style class for a {@link Gtk.Label} to emulate Pango's "<small>" and "size='smaller'"
 */
pub const STYLE_CLASS_SMALL_LABEL: &str = "small-label";
/**
 * Style class for a {@link Granite.Widgets.SourceList}
 */
pub const STYLE_CLASS_SOURCE_LIST: &str = "source-list";
/**
 * Style class for a {@link Granite.Widgets.Granite.Widgets.StorageBar}
 */
pub const STYLE_CLASS_STORAGEBAR: &str = "storage-bar";
/**
 * Style class for {@link Gtk.Label} or {@link Gtk.TextView} to emulate the appearance of Terminal. This includes
 * text color, background color, selection highlighting, and selecting the system monospace font.
 *
 * When used with {@link Gtk.Label} this style includes usizeernal padding. When used with {@link Gtk.TextView}
 * usizeeral padding will need to be set with {@link Gtk.Container.border_width}
 */
pub const STYLE_CLASS_TERMINAL: &str = "terminal";
/**
 * Style class for a {@link Granite.Widgets.Welcome}
 */
pub const STYLE_CLASS_WELCOME: &str = "welcome";
/**
 * Style class for a warmth scale, a {@link Gtk.Scale} with a "less warm" to "more warm" color gradient
 */
pub const STYLE_CLASS_WARMTH: &str = "warmth";
/**
 * Style class for a temperature scale, a {@link Gtk.Scale} with a "cold" to "hot" color gradient
 */
pub const STYLE_CLASS_TEMPERATURE: &str = "temperature";

/**
 * Transition duration when a widget closes, hides a portion of its content, or exits the screen
 */
pub const TRANSITION_DURATION_CLOSE: usize = 200;

/**
 * Transition duration when a widget opens, reveals more content, or enters the screen
 */
pub const TRANSITION_DURATION_OPEN: usize = 250;
