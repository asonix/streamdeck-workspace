use event_listener::{Event, EventListener};
use futures_core::stream::Stream;
use gtk::{prelude::*, subclass::prelude::*};
use std::{
    future::Future,
    pin::Pin,
    rc::Rc,
    task::{Context, Poll},
};

glib::wrapper! {
    pub struct AlertView(ObjectSubclass<imp::AlertView>)
        @extends gtk::Box, gtk::Container, gtk::Widget;
}

pub struct ActionStream {
    event: Rc<Event>,
    listener: Option<EventListener>,
}

impl AlertView {
    pub fn new(title: Option<&str>, description: Option<&str>, icon_name: Option<&str>) -> Self {
        let this = glib::Object::new(&[]).expect("Failed to create alert view");

        let inner = imp::AlertView::from_instance(&this);
        let widgets = inner.widgets.get().unwrap();
        if let Some(title_text) = title {
            widgets.title_label.set_label(title_text);
        }
        if let Some(description_text) = description {
            widgets.description_label.set_label(description_text);
        }
        widgets.image.set_icon_name(icon_name);

        this
    }

    pub fn action_activated(&self) -> ActionStream {
        let this = imp::AlertView::from_instance(self);

        let event = this.event.get().unwrap().clone();

        ActionStream {
            listener: Some(event.listen()),
            event,
        }
    }

    pub fn show_action(&self, label: Option<&str>) {
        let this = imp::AlertView::from_instance(self);
        let widgets = this.widgets.get().unwrap();

        if let Some(label_text) = label {
            widgets.action_button.set_label(label_text);
        }

        if widgets.action_button.label().is_none() {
            return;
        }

        widgets.action_revealer.set_reveal_child(true);
        widgets.action_revealer.show_all();
    }
}

mod imp {
    use event_listener::Event;
    use gtk::{prelude::*, subclass::prelude::*};
    use once_cell::unsync::OnceCell;
    use std::rc::Rc;

    #[derive(Debug)]
    pub(super) struct AlertWidgets {
        pub(super) title_label: gtk::Label,
        pub(super) description_label: gtk::Label,
        pub(super) image: gtk::Image,
        pub(super) action_button: gtk::Button,
        pub(super) action_revealer: gtk::Revealer,
    }

    #[derive(Debug, Default)]
    pub struct AlertView {
        pub(super) widgets: OnceCell<AlertWidgets>,
        pub(super) event: OnceCell<Rc<Event>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AlertView {
        const NAME: &'static str = "AlertView";
        type Type = super::AlertView;
        type ParentType = gtk::Box;
    }

    impl ObjectImpl for AlertView {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.style_context().add_class(*gtk::STYLE_CLASS_VIEW);

            let title_label = gtk::Label::new(None);
            title_label.set_hexpand(true);
            title_label
                .style_context()
                .add_class(crate::constants::STYLE_CLASS_H2_LABEL);
            title_label.set_max_width_chars(75);
            title_label.set_wrap(true);
            title_label.set_wrap_mode(pango::WrapMode::WordChar);
            title_label.set_xalign(0f32);

            let description_label = gtk::Label::new(None);
            description_label.set_hexpand(true);
            description_label.set_max_width_chars(75);
            description_label.set_wrap(true);
            description_label.set_use_markup(true);
            description_label.set_xalign(0f32);
            description_label.set_valign(gtk::Align::Start);

            let action_button = gtk::Button::new();
            action_button.set_margin_top(24);

            let action_revealer = gtk::Revealer::new();
            action_revealer.add(&action_button);
            action_revealer.set_halign(gtk::Align::End);
            action_revealer.set_transition_type(gtk::RevealerTransitionType::SlideUp);

            let image = gtk::Image::new();
            image.set_margin_top(6);
            image.set_valign(gtk::Align::Start);

            let layout = gtk::Grid::new();
            layout.set_column_spacing(12);
            layout.set_row_spacing(6);
            layout.set_halign(gtk::Align::Center);
            layout.set_valign(gtk::Align::Center);
            layout.set_vexpand(true);
            layout.set_margin(24);

            layout.attach(&image, 1, 1, 1, 2);
            layout.attach(&title_label, 2, 1, 1, 1);
            layout.attach(&description_label, 2, 2, 1, 1);
            layout.attach(&action_revealer, 2, 3, 1, 1);

            obj.add(&layout);

            obj.show_all();

            let event = Rc::new(Event::new());

            action_button.connect_clicked(glib::clone!(@weak obj, @weak event => move |_| {
                event.notify(usize::MAX);
            }));

            self.event.set(event).unwrap();

            self.widgets
                .set(AlertWidgets {
                    title_label,
                    description_label,
                    image,
                    action_button,
                    action_revealer,
                })
                .unwrap();
        }
    }

    impl ContainerImpl for AlertView {}
    impl WidgetImpl for AlertView {}
    impl BoxImpl for AlertView {}
}

impl Stream for ActionStream {
    type Item = ();

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let mut listener = if let Some(listener) = self.listener.take() {
            listener
        } else {
            self.event.listen()
        };

        match Pin::new(&mut listener).poll(cx) {
            Poll::Ready(_) => {
                let mut listener;
                while {
                    listener = self.event.listen();

                    matches!(Pin::new(&mut listener).poll(cx), Poll::Ready(()))
                } {}

                self.listener = Some(listener);
                Poll::Ready(Some(()))
            }
            Poll::Pending => {
                self.listener = Some(listener);
                Poll::Pending
            }
        }
    }
}
