glib::wrapper! {
    pub struct Dialog(ObjectSubclass<imp::Dialog>)
        @extends gtk::Dialog, gtk::Window, gtk::Bin, gtk::Container, gtk::Widget;
}

impl Dialog {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create dialog")
    }
}

impl Default for Dialog {
    fn default() -> Self {
        Self::new()
    }
}

mod imp {
    use gtk::{prelude::*, subclass::prelude::*};

    #[derive(Debug, Default)]
    pub struct Dialog;

    #[glib::object_subclass]
    impl ObjectSubclass for Dialog {
        const NAME: &'static str = "Dialog";
        type Type = super::Dialog;
        type ParentType = gtk::Dialog;
    }

    impl ObjectImpl for Dialog {
        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            obj.style_context().add_class("csd");

            let content_area = obj.content_area();
            let content_area_parent = content_area
                .parent()
                .unwrap()
                .downcast::<gtk::Container>()
                .unwrap();
            content_area_parent.remove(&content_area);

            let action_area = gtk::Box::new(gtk::Orientation::Horizontal, 3);

            obj.set_deletable(false);
            obj.set_window_position(gtk::WindowPosition::CenterOnParent);

            if let Some(header_bar) = obj.header_bar() {
                let header_bar_parent = header_bar
                    .parent()
                    .unwrap()
                    .downcast::<gtk::Container>()
                    .unwrap();
                header_bar_parent.remove(&header_bar);
            }

            let grid = gtk::Grid::new();
            grid.add(&content_area);
            grid.add(&action_area);
            grid.show_all();

            content_area_parent.add(&grid);

            let header_bar = gtk::HeaderBar::new();
            header_bar.set_has_subtitle(false);
            header_bar.style_context().add_class("default-decoration");
            header_bar.show();

            obj.set_titlebar(Some(&header_bar));
        }
    }

    impl WidgetImpl for Dialog {}
    impl ContainerImpl for Dialog {}
    impl BinImpl for Dialog {}
    impl WindowImpl for Dialog {}
    impl DialogImpl for Dialog {}
}
