use crate::{message::ObsMessage, store::Store};
use either::Either;
use futures_util::stream::{FuturesUnordered, StreamExt};
use obws::{
    requests::{SceneItemProperties, SceneItemSpecification},
    Client,
};
use streamdeck_common::{
    Command, ObsState, Query, QueryResponse, Scene, SceneItem, StateOperation, VisibilityOperation,
};
use tokio::sync::mpsc::{Receiver, Sender};

pub(crate) struct Obs {
    store: Store,
    inner: Inner,
}

enum Inner {
    Disconnected(Disconnected),
    Unauthenticated(Unauthenticated),
    Connected(Connected),
}

#[derive(Debug)]
struct Disconnected;

struct Unauthenticated {
    client: Option<Client>,
}

struct Connected {
    client: Option<Client>,
}

pub(crate) async fn task(store: Store, tx: Sender<ObsMessage>, mut rx: Receiver<ObsMessage>) {
    let store2 = store.clone();
    tokio::task::spawn(async move {
        if let Err(e) = connect_obs(store2, tx).await {
            tracing::info!("Failed to connect to OBS: {}", e);
        }
    });

    let mut obs = Obs {
        store,
        inner: Inner::Disconnected(Disconnected),
    };

    while let Some(msg) = rx.recv().await {
        obs.handle(msg).await;
    }
}

async fn connect_obs(store: Store, tx: Sender<ObsMessage>) -> Result<(), anyhow::Error> {
    let host = store
        .setting("obs-host")
        .await?
        .ok_or_else(|| anyhow::anyhow!("No key 'obs-host'"))?;
    let port = store
        .setting("obs-port")
        .await?
        .ok_or_else(|| anyhow::anyhow!("No key 'obs-port'"))?;

    if tx.send(ObsMessage::Connect(host, port)).await.is_err() {
        return Err(anyhow::anyhow!("Failed to send connect message"));
    }

    let (sender, rx) = tokio::sync::oneshot::channel();

    if tx.send(ObsMessage::State(sender)).await.is_err() {
        return Err(anyhow::anyhow!("Failed to send state message"));
    }

    match rx.await? {
        ObsState::Disconnected => return Err(anyhow::anyhow!("Failed to connect to obs")),
        ObsState::Connected => return Ok(()),
        ObsState::Unauthenticated => {
            let password = store
                .setting("obs-password")
                .await?
                .ok_or_else(|| anyhow::anyhow!("No stored password"))?;

            if tx.send(ObsMessage::Authenticate(password)).await.is_err() {
                return Err(anyhow::anyhow!("Failed to send authenticate message"));
            }
        }
    }

    let (sender, rx) = tokio::sync::oneshot::channel();

    if tx.send(ObsMessage::State(sender)).await.is_err() {
        return Err(anyhow::anyhow!("Failed to send state message"));
    }

    if !rx.await?.is_connected() {
        return Err(anyhow::anyhow!("Failed to authenticate with OBS"));
    }

    Ok(())
}

impl Obs {
    fn to_obs_state(&self) -> ObsState {
        match self.inner {
            Inner::Disconnected(_) => ObsState::Disconnected,
            Inner::Unauthenticated(_) => ObsState::Unauthenticated,
            Inner::Connected(_) => ObsState::Connected,
        }
    }

    async fn handle(&mut self, message: ObsMessage) {
        match message {
            ObsMessage::State(sender) => {
                let _ = sender.send(self.to_obs_state());
            }
            message => {
                self.inner.handle(message, &self.store).await;
            }
        }
    }
}

impl Inner {
    async fn handle(&mut self, message: ObsMessage, store: &Store) {
        let new_state = match self {
            Inner::Disconnected(disconnected) => disconnected.handle(message, store).await,
            Inner::Unauthenticated(unauthenticated) => unauthenticated.handle(message, store).await,
            Inner::Connected(connected) => connected.handle(message).await,
        };

        if let Some(state) = new_state {
            let _ = std::mem::replace(self, state);
        }
    }
}

impl Disconnected {
    async fn handle(&mut self, message: ObsMessage, store: &Store) -> Option<Inner> {
        match message {
            ObsMessage::Connect(host, port) => self
                .connect(host, port, store)
                .await
                .map_err(|e| tracing::error!("{}", e))
                .ok(),
            _ => None,
        }
    }

    async fn connect(
        &mut self,
        host: String,
        port: u16,
        store: &Store,
    ) -> Result<Inner, anyhow::Error> {
        tracing::info!("Connecting to {}:{}", host, port);
        let client = Client::connect(host.clone(), port).await?;
        let auth_required = client.general().get_auth_required().await?;

        store.save_setting("obs-host", host).await?;
        store.save_setting("obs-port", port).await?;

        if auth_required.auth_required {
            return Ok(Inner::Unauthenticated(Unauthenticated {
                client: Some(client),
            }));
        }

        Ok(Inner::Connected(Connected {
            client: Some(client),
        }))
    }
}

impl Unauthenticated {
    async fn handle(&mut self, message: ObsMessage, store: &Store) -> Option<Inner> {
        match message {
            ObsMessage::Authenticate(password) => self.authenticate(password, store).await.ok(),
            _ => None,
        }
    }

    async fn authenticate(
        &mut self,
        password: String,
        store: &Store,
    ) -> Result<Inner, anyhow::Error> {
        if let Some(client) = self.client.as_ref() {
            if let Err(e) = client.login(Some(password.clone())).await {
                if client.general().get_version().await.is_ok() {
                    return Err(e.into());
                } else {
                    return Ok(Inner::Disconnected(Disconnected));
                }
            }
        }

        store.save_setting("obs-password", password).await?;

        Ok(Inner::Connected(Connected {
            client: self.client.take(),
        }))
    }
}

impl Connected {
    async fn handle(&mut self, message: ObsMessage) -> Option<Inner> {
        match message {
            ObsMessage::Command(command) => {
                let res = self.command(command).await;
                self.check_err(res).await
            }
            ObsMessage::Query(sender, query) => {
                let res = self.query(query).await.map(|response| {
                    let _ = sender.send(response);
                });
                self.check_err(res).await
            }
            ObsMessage::Disconnect => {
                let _ = self.disconnect().await;
                Some(Inner::Disconnected(Disconnected))
            }
            _ => None,
        }
    }

    async fn check_err(&mut self, res: Result<(), anyhow::Error>) -> Option<Inner> {
        match res {
            Ok(()) => None,
            Err(_) => {
                if let Some(client) = self.client.as_ref() {
                    if client.general().get_version().await.is_ok() {
                        return None;
                    }
                }

                Some(Inner::Disconnected(Disconnected))
            }
        }
    }

    async fn command(&self, command: Command) -> Result<(), anyhow::Error> {
        let client = if let Some(client) = self.client.as_ref() {
            client
        } else {
            return Err(anyhow::anyhow!("no client"));
        };

        match command {
            Command::SwitchScene { to } => {
                client.scenes().set_current_scene(&to).await?;
            }
            Command::SetSceneItemVisibility {
                scene_name,
                item_id,
                operation,
            } => {
                let visible = match operation {
                    VisibilityOperation::On => true,
                    VisibilityOperation::Off => false,
                    VisibilityOperation::Toggle => {
                        let properties = client
                            .scene_items()
                            .get_scene_item_properties(
                                Some(&scene_name),
                                Either::Right(SceneItemSpecification::<'static> {
                                    name: None,
                                    id: Some(item_id),
                                }),
                            )
                            .await?;

                        !properties.visible
                    }
                };

                client
                    .scene_items()
                    .set_scene_item_properties(SceneItemProperties {
                        scene_name: Some(&scene_name),
                        item: Either::Right(SceneItemSpecification {
                            name: None,
                            id: Some(item_id),
                        }),
                        visible: Some(visible),
                        ..Default::default()
                    })
                    .await?;
            }
            Command::SetStreaming { operation } => match operation {
                StateOperation::Start => {
                    let status = client.streaming().get_streaming_status().await?;

                    if !status.streaming {
                        client.streaming().start_streaming(None).await?;
                    }
                }
                StateOperation::Stop => {
                    let status = client.streaming().get_streaming_status().await?;

                    if status.streaming {
                        client.streaming().stop_streaming().await?;
                    }
                }
                StateOperation::Toggle => {
                    client.streaming().start_stop_streaming().await?;
                }
            },
            Command::SetRecording { operation } => match operation {
                StateOperation::Start => {
                    let status = client.recording().get_recording_status().await?;

                    if !status.is_recording {
                        client.recording().start_recording().await?;
                    }
                }
                StateOperation::Stop => {
                    let status = client.recording().get_recording_status().await?;

                    if status.is_recording {
                        client.recording().stop_recording().await?;
                    }
                }
                StateOperation::Toggle => {
                    client.recording().start_stop_recording().await?;
                }
            },
        }

        Ok(())
    }

    async fn query(&self, query: Query) -> Result<QueryResponse, anyhow::Error> {
        let client = if let Some(client) = self.client.as_ref() {
            client
        } else {
            return Err(anyhow::anyhow!("no client"));
        };

        match query {
            Query::GetScenes => {
                let scene_list = client.scenes().get_scene_list().await?;

                let scenes = scene_list
                    .scenes
                    .into_iter()
                    .map(|s| Scene { name: s.name })
                    .collect();
                Ok(QueryResponse::Scenes { scenes })
            }
            Query::GetSceneItems { scene_name } => {
                let scene_item_list = client
                    .scene_items()
                    .get_scene_item_list(Some(&scene_name))
                    .await?;

                let mut unordered = FuturesUnordered::new();
                for item in scene_item_list.scene_items {
                    let scene_name = scene_name.clone();
                    unordered.push(async move {
                        client
                            .scene_items()
                            .get_scene_item_properties(
                                Some(&scene_name),
                                Either::Right(SceneItemSpecification::<'static> {
                                    name: None,
                                    id: Some(item.item_id),
                                }),
                            )
                            .await
                    });
                }

                let mut scene_items = Vec::new();
                while let Some(res) = unordered.next().await {
                    let properties = res?;
                    scene_items.push(SceneItem {
                        id: properties.item_id,
                        name: properties.name,
                        visible: properties.visible,
                    });
                }

                Ok(QueryResponse::SceneItems { items: scene_items })
            }
            Query::GetSceneItem {
                scene_name,
                item_id,
            } => {
                let properties = client
                    .scene_items()
                    .get_scene_item_properties(
                        Some(&scene_name),
                        Either::Right(SceneItemSpecification::<'static> {
                            name: None,
                            id: Some(item_id),
                        }),
                    )
                    .await?;

                Ok(QueryResponse::SceneItem {
                    item: SceneItem {
                        id: properties.item_id,
                        name: properties.name,
                        visible: properties.visible,
                    },
                })
            }
        }
    }

    async fn disconnect(&mut self) -> Result<(), anyhow::Error> {
        if let Some(client) = self.client.as_mut() {
            client.disconnect().await;
        }
        Ok(())
    }
}
