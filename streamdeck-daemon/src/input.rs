use crate::{
    message::{InputMessage, ObsMessage},
    store::Store,
};
use tokio::sync::{broadcast::Receiver, mpsc::Sender};

pub(crate) async fn task(store: Store, mut rx: Receiver<InputMessage>, obs_tx: Sender<ObsMessage>) {
    while let Ok((serial_number, keys)) = rx.recv().await {
        tracing::debug!("{}: {}", serial_number, keys);

        if let Ok(Some(command)) = store.command(&serial_number, keys).await {
            let _ = obs_tx.send(ObsMessage::Command(command)).await;
        }
    }
}
