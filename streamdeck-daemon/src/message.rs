use crate::port::{DeckConfig, Port};
pub use streamdeck_common::Input;
use streamdeck_common::{Command, ObsState, Query, QueryResponse};
use tokio::sync::oneshot::Sender;

#[derive(Debug)]
pub(crate) enum ObsMessage {
    Query(Sender<QueryResponse>, Query),
    Command(Command),
    Connect(String, u16),
    Authenticate(String),
    State(Sender<ObsState>),
    Disconnect,
}

pub(crate) type InputMessage = (String, Input);

pub(crate) enum DeckMessage {
    Open(DeckConfig),
    Close(String),
}

#[derive(Debug)]
pub(crate) enum ManagerMessage {
    EnableDiscovery,
    DisableDiscovery,
    Tick,
    Found(Vec<String>),
    Opened(DeckConfig, Port),
    Closed(String),
    InvalidPort(String),
}
