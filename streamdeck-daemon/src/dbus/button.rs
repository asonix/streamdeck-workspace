use crate::{dbus::button_path, store::Store};
use streamdeck_common::{Command, Input};
use zbus::{Connection, SignalContext};

pub(super) struct Button {
    serial_number: String,
    keys: Input,
    command: String,
    name: String,

    store: Store,
}

impl Button {
    #[tracing::instrument(skip(connection))]
    pub(super) async fn hydrate(
        connection: Connection,
        serial_number: String,
        keys: Input,
        store: Store,
    ) -> anyhow::Result<()> {
        let command_opt = store.command(&serial_number, keys.clone()).await?;

        let command = command_opt.unwrap_or(Command::SetStreaming {
            operation: streamdeck_common::StateOperation::Toggle,
        });

        let command = serde_json::to_string(&command)?;

        let name_opt = store.input_name(&serial_number, keys.clone()).await?;

        let name = name_opt.unwrap_or_else(|| String::from("Streamdeck"));

        let path = button_path(&serial_number, &keys);

        let button = Button {
            serial_number,
            keys,
            command,
            name,
            store,
        };

        let _ = connection.object_server().at(path, button).await?;

        Ok(())
    }
}

#[zbus::dbus_interface(name = "dog.asonix.git.asonix.StreamdeckDaemon.Deck.Button")]
impl Button {
    #[dbus_interface(property)]
    async fn name(&self) -> &str {
        tracing::debug!("name");
        &self.name
    }

    #[dbus_interface(property)]
    async fn set_name(&mut self, name: String) {
        tracing::debug!("set name");
        match self.do_set_name(&name).await {
            Ok(()) => self.name = name,
            Err(e) => {
                tracing::warn!("Failed to set name: {}", e);
            }
        }
    }

    #[dbus_interface(property)]
    async fn command(&self) -> &str {
        tracing::debug!("command");
        &self.command
    }

    #[dbus_interface(property)]
    pub(super) async fn set_command(&mut self, command: String) {
        tracing::debug!("set command");
        match self.do_set_command(&command).await {
            Ok(()) => self.command = command,
            Err(e) => {
                tracing::warn!("Failed to set command: {}", e);
            }
        }
    }

    #[dbus_interface(signal)]
    pub(super) async fn pushed(ctx: &SignalContext<'_>) -> zbus::Result<()>;
}

impl Button {
    async fn do_set_name(&self, name: &str) -> anyhow::Result<()> {
        self.store
            .set_input_name(&self.serial_number, self.keys.clone(), name)
            .await?;
        Ok(())
    }

    async fn do_set_command(&self, command: &str) -> anyhow::Result<()> {
        let command: Command = serde_json::from_str(command)?;
        self.store
            .store(&self.serial_number, self.keys.clone(), &command)
            .await?;
        Ok(())
    }
}
