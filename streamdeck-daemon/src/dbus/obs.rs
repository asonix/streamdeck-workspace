use crate::{dbus::obs_path, message::ObsMessage};
use streamdeck_common::ObsState;
use tokio::sync::mpsc::Sender;
use zbus::Connection;

pub(crate) struct Obs {
    connection: Connection,
    state: String,
    obs: Sender<ObsMessage>,
}

impl Obs {
    #[tracing::instrument(skip_all)]
    pub(crate) async fn hydrate(
        connection: Connection,
        obs: Sender<ObsMessage>,
    ) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();

        obs.send(ObsMessage::State(tx)).await?;
        let state = rx.await?;

        let obs = Self {
            connection: connection.clone(),
            obs,
            state: state.to_string(),
        };

        connection.object_server().at(obs_path(), obs).await?;

        Ok(())
    }
}

#[zbus::dbus_interface(name = "dog.asonix.git.asonix.StreamdeckDaemon.Obs")]
impl Obs {
    #[dbus_interface(property)]
    async fn state(&self) -> &str {
        &self.state
    }

    async fn connect(&mut self, host: String, port: u16) -> zbus::fdo::Result<()> {
        tracing::debug!("connect");
        self.do_connect(host, port)
            .await
            .map_err(crate::dbus::fail)?;

        Ok(())
    }

    async fn disconnect(&mut self) -> zbus::fdo::Result<()> {
        tracing::debug!("disconnect");
        self.do_disconnect().await.map_err(crate::dbus::fail)?;

        Ok(())
    }

    async fn login(&mut self, password: String) -> zbus::fdo::Result<()> {
        tracing::debug!("login");
        self.do_login(password).await.map_err(crate::dbus::fail)?;

        Ok(())
    }

    async fn query(&self, query: &str) -> zbus::fdo::Result<String> {
        tracing::debug!("query");
        self.do_query(query).await.map_err(crate::dbus::fail)
    }

    async fn test(&self, command: &str) -> zbus::fdo::Result<()> {
        tracing::debug!("test");

        self.do_test(command).await.map_err(crate::dbus::fail)
    }
}

impl Obs {
    async fn set_state(&mut self, state: ObsState) -> anyhow::Result<()> {
        let state = state.to_string();
        if self.state != state {
            self.state = state;

            let iface = self
                .connection
                .object_server()
                .interface::<_, Self>(obs_path())
                .await?;

            let signal_context = iface.signal_context();

            self.state_changed(signal_context).await?;
        }

        Ok(())
    }

    async fn do_connect(&mut self, host: String, port: u16) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();

        self.obs.send(ObsMessage::Connect(host, port)).await?;
        self.obs.send(ObsMessage::State(tx)).await?;
        let state = rx.await?;

        self.set_state(state).await?;
        Ok(())
    }

    async fn do_disconnect(&mut self) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();

        self.obs.send(ObsMessage::Disconnect).await?;
        self.obs.send(ObsMessage::State(tx)).await?;
        let state = rx.await?;

        self.set_state(state).await?;
        Ok(())
    }

    async fn do_login(&mut self, password: String) -> anyhow::Result<()> {
        let (tx, rx) = tokio::sync::oneshot::channel();

        self.obs.send(ObsMessage::Authenticate(password)).await?;
        self.obs.send(ObsMessage::State(tx)).await?;
        let state = rx.await?;

        self.set_state(state).await?;
        Ok(())
    }

    async fn do_query(&self, query: &str) -> anyhow::Result<String> {
        let (tx, rx) = tokio::sync::oneshot::channel();

        let query = serde_json::from_str(query)?;

        self.obs.send(ObsMessage::Query(tx, query)).await?;
        let response = rx.await?;
        let response = serde_json::to_string(&response)?;

        Ok(response)
    }

    async fn do_test(&self, command: &str) -> anyhow::Result<()> {
        let command = serde_json::from_str(command)?;
        self.obs.send(ObsMessage::Command(command)).await?;
        Ok(())
    }
}
