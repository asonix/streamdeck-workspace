use crate::{
    deck::Deck,
    message::{DeckMessage, InputMessage, ManagerMessage},
    port::{DeckConfig, Port},
};
use std::{
    collections::{HashMap, HashSet},
    future::Future,
    pin::Pin,
    task::{Context, Poll},
};
use tokio::{
    sync::{
        broadcast,
        mpsc::{Receiver, Sender},
    },
    task::JoinHandle,
    time::Duration,
};

struct Manager {
    input_tx: broadcast::Sender<InputMessage>,
    deck_tx: Sender<DeckMessage>,
    tx: Sender<ManagerMessage>,
    decks: HashMap<String, DeckConfig>,
    ports: HashMap<String, JoinHandle<()>>,
    opening: HashMap<String, JoinHandle<()>>,
    invalid: HashSet<String>,
    finding: Option<JoinHandle<()>>,
    discovery: Option<JoinHandle<()>>,
}

impl Manager {
    fn new(
        input_tx: broadcast::Sender<InputMessage>,
        tx: Sender<ManagerMessage>,
        deck_tx: Sender<DeckMessage>,
    ) -> Self {
        let discovery = tokio::spawn(tick_task(tx.clone()));
        Manager {
            input_tx,
            deck_tx,
            tx,
            decks: HashMap::default(),
            ports: HashMap::default(),
            opening: HashMap::default(),
            invalid: HashSet::default(),
            finding: None,
            discovery: Some(discovery),
        }
    }

    fn turn(&mut self, msg: ManagerMessage) -> Turn<'_> {
        Turn(self, Some(msg))
    }

    fn do_turn(&mut self, msg: ManagerMessage, cx: &mut Context<'_>) {
        tracing::debug!("msg: {:?}", msg);
        tracing::debug!("state: {:?}", self);
        match msg {
            ManagerMessage::EnableDiscovery => {
                if let Some(mut handle) = self.discovery.take() {
                    if matches!(Pin::new(&mut handle).poll(cx), Poll::Pending) {
                        self.discovery = Some(handle);
                        return;
                    }
                }

                let handle = tokio::spawn(tick_task(self.tx.clone()));
                self.discovery = Some(handle);
            }
            ManagerMessage::DisableDiscovery => {
                if let Some(handle) = self.discovery.take() {
                    handle.abort();
                }
            }
            ManagerMessage::Tick => {
                if let Some(mut handle) = self.finding.take() {
                    if matches!(Pin::new(&mut handle).poll(cx), Poll::Pending) {
                        self.finding = Some(handle);
                        return;
                    }
                }

                let handle = tokio::spawn(find_task(self.tx.clone()));
                self.finding = Some(handle);
            }
            ManagerMessage::Found(found_decks) => {
                let known = self.ports.keys().cloned().collect::<HashSet<_>>();
                let found = found_decks.iter().cloned().collect::<HashSet<_>>();

                for port_name in found.difference(&known) {
                    if let Some(mut handle) = self.opening.remove(port_name) {
                        if matches!(Pin::new(&mut handle).poll(cx), Poll::Pending) {
                            self.opening.insert(port_name.clone(), handle);
                            continue;
                        }
                    }

                    if self.invalid.contains(port_name) {
                        continue;
                    }
                    let handle = tokio::spawn(open_task(port_name.clone(), self.tx.clone()));
                    self.opening.insert(port_name.clone(), handle);
                }

                for port_name in known.difference(&found) {
                    tracing::info!("Removed deck: {}", port_name);
                    if let Some(handle) = self.ports.remove(port_name) {
                        handle.abort();
                    }
                    self.decks.remove(port_name);
                    self.invalid.remove(port_name);
                }
            }
            ManagerMessage::Opened(deck, port) => {
                tracing::info!("New deck: {}", deck.port_name);
                let deck_tx = self.deck_tx.clone();
                let deck_fut = Deck {
                    manager: self.tx.clone(),
                    input: self.input_tx.clone(),
                    port,
                    config: deck.clone(),
                }
                .run();
                let deck2 = deck.clone();
                let handle = tokio::spawn(async move {
                    let _ = deck_tx.send(DeckMessage::Open(deck2)).await;
                    deck_fut.await
                });
                self.ports.insert(deck.port_name.clone(), handle);
                self.decks.insert(deck.port_name.clone(), deck);
            }
            ManagerMessage::InvalidPort(port_name) => {
                self.invalid.insert(port_name);
            }
            ManagerMessage::Closed(port_name) => {
                if let Some(handle) = self.ports.remove(&port_name) {
                    handle.abort();
                }
                if let Some(deck) = self.decks.remove(&port_name) {
                    let deck_tx = self.deck_tx.clone();
                    tokio::spawn(async move {
                        let _ = deck_tx.send(DeckMessage::Close(deck.serial_number)).await;
                    });
                }
            }
        }
    }
}

pub(crate) async fn task(
    input_tx: broadcast::Sender<InputMessage>,
    deck_tx: Sender<DeckMessage>,
    tx: Sender<ManagerMessage>,
    mut rx: Receiver<ManagerMessage>,
) {
    let mut state = Manager::new(input_tx, tx, deck_tx);

    while let Some(msg) = rx.recv().await {
        state.turn(msg).await;
    }
}

struct Turn<'a>(&'a mut Manager, Option<ManagerMessage>);

impl<'a> Future for Turn<'a> {
    type Output = ();

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let msg = self.1.take().unwrap();
        self.0.do_turn(msg, cx);
        Poll::Ready(())
    }
}

async fn tick_task(tx: Sender<ManagerMessage>) {
    let mut interval = tokio::time::interval(Duration::from_secs(5));

    loop {
        let _ = tx.send(ManagerMessage::Tick).await;
        interval.tick().await;
    }
}

async fn find_task(tx: Sender<ManagerMessage>) {
    match find_streamdecks().await {
        Ok(streamdecks) => {
            let _ = tx.send(ManagerMessage::Found(streamdecks)).await;
        }
        Err(e) => tracing::error!("Error finding streamdecks: {}", e),
    }
}

async fn open_task(port_name: String, tx: Sender<ManagerMessage>) {
    let msg = match Port::open(port_name.clone()).await {
        Ok(mut port) => match port.config().await {
            Ok(config) => ManagerMessage::Opened(config, port),
            Err(error) => {
                tracing::warn!("Failed to get port config: {}", error);
                return;
            }
        },
        Err(_) => ManagerMessage::InvalidPort(port_name),
    };

    let _ = tx.send(msg).await;
}

async fn find_streamdecks() -> Result<Vec<String>, anyhow::Error> {
    tokio::task::spawn_blocking(|| {
        Ok(tokio_serial::available_ports()?
            .into_iter()
            .map(|port| port.port_name)
            .collect())
    })
    .await?
}

impl std::fmt::Debug for Manager {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let ports = self.ports.keys().map(|s| s.as_str()).collect::<Vec<_>>();

        f.debug_struct("Manager").field("ports", &ports).finish()
    }
}
