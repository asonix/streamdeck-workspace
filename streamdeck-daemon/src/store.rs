use sled::{Db, IVec, Tree};
use streamdeck_common::Command;

use crate::message::Input;

#[derive(Clone)]
pub(crate) struct Store {
    commands: Tree,
    settings: Tree,
    names: Tree,
    #[allow(unused)]
    db: Db,
}

impl std::fmt::Debug for Store {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Store")
            .field("commands", &"Tree")
            .field("settings", &"Tree")
            .field("names", &"Tree")
            .field("db", &"Db")
            .finish()
    }
}

impl Store {
    pub(crate) async fn build(db: Db) -> Result<Self, anyhow::Error> {
        tokio::task::spawn_blocking(move || {
            Ok(Store {
                commands: db.open_tree("dog.asonix.git.asonix.streamdeck/commands")?,
                settings: db.open_tree("dog.asonix.git.asonix.streamdeck/settings")?,
                names: db.open_tree("dog.asonix.git.asonix.streamdeck/names")?,
                db,
            }) as Result<Store, anyhow::Error>
        })
        .await?
    }

    pub(crate) async fn set_input_name(
        &self,
        deck: &str,
        input: Input,
        name: &str,
    ) -> Result<(), anyhow::Error> {
        let key = self.input_name_key(deck, input);
        let value = name.as_bytes().to_vec();

        let names = self.names.clone();
        tokio::task::spawn_blocking(move || names.insert(key, value)).await??;
        Ok(())
    }

    pub(crate) async fn input_name(
        &self,
        deck: &str,
        input: Input,
    ) -> anyhow::Result<Option<String>> {
        let key = self.input_name_key(deck, input);

        let names = self.names.clone();
        let value = tokio::task::spawn_blocking(move || names.get(key)).await??;

        if let Some(value) = value {
            Ok(Some(String::from_utf8_lossy(&value).to_string()))
        } else {
            Ok(None)
        }
    }

    pub(crate) async fn set_deck_name(&self, deck: &str, name: &str) -> Result<(), anyhow::Error> {
        let key = self.deck_name_key(deck);
        let value = name.as_bytes().to_vec();

        let names = self.names.clone();
        tokio::task::spawn_blocking(move || names.insert(key, value)).await??;
        Ok(())
    }

    pub(crate) async fn deck_name(&self, deck: &str) -> Result<Option<String>, anyhow::Error> {
        let key = self.deck_name_key(deck);

        let names = self.names.clone();
        let opt = tokio::task::spawn_blocking(move || names.get(key)).await??;

        if let Some(ivec) = opt {
            return Ok(Some(String::from_utf8_lossy(&ivec).to_string()));
        }

        Ok(None)
    }

    pub(crate) async fn setting<T>(&self, key: &str) -> Result<Option<T>, anyhow::Error>
    where
        T: serde::de::DeserializeOwned,
    {
        let key = key.to_owned();
        let settings = self.settings.clone();

        let opt = tokio::task::spawn_blocking(move || settings.get(key)).await??;

        if let Some(ivec) = opt {
            return Ok(Some(serde_json::from_slice(&ivec)?));
        }

        Ok(None)
    }

    pub(crate) async fn save_setting<T>(&self, key: &str, value: T) -> Result<(), anyhow::Error>
    where
        T: serde::Serialize,
    {
        let key = key.to_owned();
        let value = serde_json::to_vec(&value)?;
        let settings = self.settings.clone();

        tokio::task::spawn_blocking(move || settings.insert(key, value)).await??;

        Ok(())
    }

    pub(crate) async fn unset(&self, deck: &str, input: Input) -> Result<(), anyhow::Error> {
        let key = self.cmd_key(deck, input);

        let commands = self.commands.clone();
        tokio::task::spawn_blocking(move || commands.remove(key)).await??;
        Ok(())
    }

    pub(crate) async fn store(
        &self,
        deck: &str,
        input: Input,
        command: &Command,
    ) -> Result<(), anyhow::Error> {
        let key = self.cmd_key(deck, input);
        let value = self.cmd_value(command)?;

        let commands = self.commands.clone();
        tokio::task::spawn_blocking(move || commands.insert(key, value)).await??;
        Ok(())
    }

    pub(crate) async fn command(
        &self,
        deck: &str,
        input: Input,
    ) -> anyhow::Result<Option<Command>> {
        let key = self.cmd_key(deck, input);

        let commands = self.commands.clone();
        let opt = tokio::task::spawn_blocking(move || commands.get(key)).await??;

        if let Some(ivec) = opt {
            return Ok(Some(serde_json::from_slice(&ivec)?));
        }

        Ok(None)
    }

    pub(crate) async fn get_commands(
        &self,
        deck: &str,
    ) -> Result<Vec<(Input, Command)>, anyhow::Error> {
        let prefix = self.cmd_prefix(deck);

        let commands = self.commands.clone();
        let vec = tokio::task::spawn_blocking(move || {
            let prefix_len = prefix.len();
            commands
                .scan_prefix(prefix)
                .filter_map(|res| {
                    let (key, value) = res.ok()?;

                    let input = Input::from_slice(&key[prefix_len..]);

                    let command = serde_json::from_slice(&value).ok()?;

                    Some((input, command))
                })
                .collect()
        })
        .await?;

        Ok(vec)
    }

    fn deck_name_key(&self, deck: &str) -> IVec {
        let mut key = b"names/".to_vec();
        key.extend(deck.as_bytes());

        IVec::from(key)
    }

    fn input_name_key(&self, deck: &str, input: Input) -> IVec {
        let mut key = b"names/".to_vec();
        key.extend(deck.as_bytes());
        key.push(b'/');
        key.extend_from_slice(&input);

        IVec::from(key)
    }

    fn cmd_prefix(&self, deck: &str) -> IVec {
        let mut prefix = deck.as_bytes().to_vec();
        prefix.push(b'/');

        IVec::from(prefix)
    }

    fn cmd_key(&self, deck: &str, input: Input) -> IVec {
        let mut key = deck.as_bytes().to_vec();
        key.push(b'/');
        key.extend_from_slice(&input);

        IVec::from(key)
    }

    fn cmd_value(&self, command: &Command) -> Result<IVec, anyhow::Error> {
        Ok(IVec::from(serde_json::to_vec(&command)?))
    }
}
