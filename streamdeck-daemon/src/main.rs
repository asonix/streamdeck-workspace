use directories::ProjectDirs;
use std::sync::Arc;
use tokio::sync::Notify;

#[cfg(feature = "ipc-dbus")]
mod dbus;
mod deck;
mod input;
mod manager;
mod message;
mod obs;
mod port;
mod store;

use store::Store;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    init_tracing()?;

    let (manager_tx, manager_rx) = tokio::sync::mpsc::channel(16);
    let (input_tx, input_rx) = tokio::sync::broadcast::channel(16);
    let (obs_tx, obs_rx) = tokio::sync::mpsc::channel(16);
    let (deck_tx, deck_rx) = tokio::sync::mpsc::channel(16);

    let project_dirs = ProjectDirs::from("dog", "asonix", "Streamdeck")
        .ok_or_else(|| anyhow::anyhow!("No home directory found"))?;
    let mut sled_dir = project_dirs.data_local_dir().to_owned();
    sled_dir.push("sled");
    sled_dir.push("db-0.34.6");

    let db = sled::Config::new().path(sled_dir).open()?;
    let store = Store::build(db).await?;

    let shutdown = Arc::new(Notify::new());

    let mut handles = vec![];

    tracing::info!("Spawning application");

    handles.push(tokio::spawn(input::task(
        store.clone(),
        input_tx.subscribe(),
        obs_tx.clone(),
    )));
    tracing::info!("Spawned input task");
    handles.push(tokio::spawn(obs::task(
        store.clone(),
        obs_tx.clone(),
        obs_rx,
    )));
    tracing::info!("Spawned obs task");
    handles.push(tokio::spawn(manager::task(
        input_tx,
        deck_tx,
        manager_tx.clone(),
        manager_rx,
    )));
    tracing::info!("Spawned manager task");

    #[cfg(feature = "ipc-dbus")]
    {
        dbus::spawn(store, input_rx, deck_rx, obs_tx, manager_tx).await?;
        tracing::info!("spawned dbus task");
    }

    tracing::info!("Spawned application");

    tokio::select! {
        _ = shutdown.notified() => {},
        _ = tokio::signal::ctrl_c() => {},
    };

    tracing::info!("Application closing");

    for hdnl in &handles {
        hdnl.abort();
    }

    for hdnl in handles {
        let _ = hdnl.await;
    }

    Ok(())
}

fn init_tracing() -> anyhow::Result<()> {
    use tracing::subscriber::set_global_default;
    use tracing_error::ErrorLayer;
    use tracing_log::LogTracer;
    use tracing_subscriber::{
        filter::Targets, fmt::format::FmtSpan, layer::SubscriberExt, Layer, Registry,
    };

    LogTracer::init()?;

    let targets = std::env::var("RUST_LOG")
        .unwrap_or_else(|_| "streamdeck_daemon=debug,streamdeck_handshake=debug,info".into())
        .parse::<Targets>()?;

    let format_layer = tracing_subscriber::fmt::layer()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_filter(targets);

    let subscriber = Registry::default()
        .with(format_layer)
        .with(ErrorLayer::default());

    set_global_default(subscriber)?;

    Ok(())
}
