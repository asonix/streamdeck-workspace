use crate::{
    message::{InputMessage, ManagerMessage},
    port::{DeckConfig, Port},
};
use tokio::{
    sync::{broadcast, mpsc::Sender},
    time::Duration,
};

#[derive(Debug)]
pub(crate) struct Deck {
    pub(crate) manager: Sender<ManagerMessage>,
    pub(crate) input: broadcast::Sender<InputMessage>,
    pub(crate) port: Port,
    pub(crate) config: DeckConfig,
}

impl Deck {
    pub(crate) async fn run(self) {
        let Deck {
            manager,
            input,
            port,
            config,
        } = self;

        io_task(port, input, config.serial_number.clone()).await;

        tracing::info!("{} disconnected", config.product_name);

        while manager
            .send(ManagerMessage::Closed(config.port_name.clone()))
            .await
            .is_err()
        {
            if manager.is_closed() {
                return;
            }

            tokio::time::sleep(Duration::from_secs(5)).await;
        }
    }
}

async fn io_task(port: Port, sender: broadcast::Sender<InputMessage>, serial_number: String) {
    let (mut read_port, _write_port) = port.split();

    loop {
        tracing::trace!("read loop");
        match read_port.read_keys().await {
            Ok(input) => {
                let _ = sender.send((serial_number.clone(), input));
            }
            Err(e) => {
                tracing::trace!("Error reading: {}", e);
                if matches!(e.kind(), std::io::ErrorKind::BrokenPipe) {
                    break;
                }
            }
        }
    }
}
